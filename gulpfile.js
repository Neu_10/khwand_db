var gulp        = require('gulp'),
    gutil       = require('gulp-util'),
    sourcemaps  = require('gulp-sourcemaps'),
    minifyCSS   = require('gulp-minify-css'),
    concat      = require('gulp-concat'),
    uglify      = require('gulp-uglify'),
    strip       = require('gulp-strip-comments'),
    jslint      = require('gulp-jslint'),
    historyApiFallback = require('connect-history-api-fallback'),
    del         = require('del'),
    bs          = require('browser-sync').create(),
    reload      = bs.reload;




/*********************************************************************************************************
CONFIGURATIONS
 ********************************************************************************************************/

var srcDir = 'src/',
    buildDir = 'dist/';

var sassOptions = {
    outputStyle: 'compressed'
};

var config = {
    jsPlugins: {
        src: [
            'bower_components/jquery/jquery.min.js',
            'bower_components/bootstrap/dist/js/bootstrap.min.js',
            'bower_components/angular/angular.min.js',
            'bower_components/angular-animate/angular-animate.min.js',
            'bower_components/angular-sanitize/angular-sanitize.min.js',
            'bower_components/angular-touch/angular-touch.min.js',
            'bower_components/angular-bootstrap/ui-bootstrap.min.js',
            'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
            'bower_components/angular-ui-select/dist/select.min.js',
            'bower_components/angular-ui-router/release/angular-ui-router.min.js',
            'bower_components/oclazyload/dist/ocLazyLoad.min.js',
            'bower_components/angular-translate/angular-translate.js',
            'bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.js',
            'bower_components/moment/min/moment.min.js',
            'bower_components/firebase/firebase.js',
            'bower_components/angularfire/dist/angularfire.min.js',
            'bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js',
            'bower_components/angular-bootstrap-switch/dist/angular-bootstrap-switch.min.js',
            'bower_components/underscore/underscore-min.js',
            'bower_components/ngstorage/ngStorage.min.js',
            'bower_components/jquery-ui/jquery-ui.min.js',
            'bower_components/jquery-migrate/jquery-migrate.min.js',
            'bower_components/select2/dist/js/select2.min.js',
            'bower_components/datatables/media/js/jquery.dataTables.min.js',
            'bower_components/angular-datatables/dist/angular-datatables.min.js',
            'bower_components/angular-datatables/dist/plugins/bootstrap/angular-datatables.bootstrap.min.js',
            'bower_components/blockUI/jquery.blockUI.js',
            'bower_components/angular-google-maps/dist/angular-google-maps.min.js',
            'bower_components/angular-xeditable/dist/js/xeditable.min.js',
            'bower_components/lodash/dist/lodash.min.js',
            /*'bower_components/jquery.uniform/dist/js/jquery.uniform.standalone.js'*/
            /*'bower_components/uniform/jquery.uniform.js',*/
            'bower_components/angular-ui-calendar/src/calendar.js',
            'bower_components/fullcalendar/dist/fullcalendar.min.js',
            'bower_components/fullcalendar/dist/gcal.js',
            'bower_components/js-cookie/src/js.cookie.js',
            'bower_components/jquery-slimscroll/jquery.slimscroll.min.js',
            'bower_components/ng-backstretch/dist/ng-backstretch.min.js',
            'bower_components/angular-simple-logger/dist/angular-simple-logger.min.js',
            'bower_components/bootstrap-select/dist/js/bootstrap-select.min.js',
            'bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
            'bower_components/ng-file-upload/ng-file-upload-shim.min.js',
            'bower_components/ng-file-upload/ng-file-upload.min.js',
            'bower_components/angularUtils-pagination/dirPagination.js',
            'bower_components/checklist-model/checklist-model.js',
            'bower_components/bootstrap-daterangepicker/daterangepicker.js',
            'bower_components/angular-daterangepicker/js/angular-daterangepicker.js',
            'bower_components/ngmap/build/scripts/ng-map.js',
            srcDir+'js/components-bootstrap-select.js',
            srcDir+'js/components-select2.js',
            srcDir+'js/theme.js',
            srcDir+'js/layout.js',
            srcDir+'js/components-dropdowns.js',
            srcDir+'js/components-pickers.js',
            srcDir+'js/form-wizard.js'
            ],
        build: buildDir+'js'
    },
    js: {
        src: [
             srcDir+'app/app.js',
             srcDir+'app/directives/*.js',
             srcDir+'app/controllers/**/*.js',
             srcDir+'app/factories/*.js',
             srcDir+'app/filters/*.js',
             srcDir+'app/models/*.js',
             srcDir+'app/services/*.js',
             srcDir+'app/router.js'
        ],
        build: buildDir+'js'
    },
    cssPlugins: {
        src: [
            'bower_components/bootstrap/dist/css/bootstrap.min.css',
            'bower_components/font-awesome/css/font-awesome.min.css',
            'bower_components/bootstrap-select/dist/css/bootstrap-select.min.css',
            'bower_components/multiselect/css/multi-select.css',
            'bower_components/simple-line-icons/css/simple-line-icons.css',
            'bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
            'bower_components/select2/dist/css/select2.min.css',
            'bower_components/angular-ui-select/dist/select.min.css',
            'bower_components/angular-xeditable/dist/css/xeditable.css',
            /*'bower_components/jquery.uniform/dist/css/default.css',*/
          /*  'bower_components/uniform/css/uniform.default.css',*/
            'bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css'
        ],
        build: buildDir+'css',
        output: 'plugins.css',
        css: srcDir+'css'
    },
    css: {
        src: [
            srcDir+'css/layout.css',
            srcDir+'css/darkblue.css',
            srcDir+'css/faq.css',
            srcDir+'css/contact.css',
            srcDir+'css/error.css',
            srcDir+'css/login.css',
            srcDir+'css/components-rounded.css',
            srcDir+'css/custom.css'
        ],
        build: buildDir+'css',
        output: 'steamy.css',
        css: srcDir+'css'
    },
    fonts: {
        src: [
            'src/fonts/*.*',
            'bower_components/bootstrap/dist/fonts/*.*',
            'bower_components/font-awesome/fonts/*.*',
            'bower_components/simple-line-icons/fonts/*.*'
        ],
        build: buildDir+'fonts/'
    },
    images: {
        src: [
            'src/images/*.png', 
            'src/images/**/*.{png,jpg,gif,svg}'
        ],
        build: 'dist/images'
    },
    templates: {
        src: [
            srcDir+'templates/**/*.*'
        ],
        build: buildDir+'templates'
    },
    pages: {
        src: [
            srcDir+'pages/*.*'
        ],
        build: buildDir+'pages'
    },
    config: {
        src: [srcDir+'config/**',
              srcDir+'config/*.*'],
        build: buildDir+'config/'
    },
    index: {
        src: [
            srcDir+'index.html',
            srcDir+'favicon.ico',
            srcDir+'sw.js',
            srcDir+'terms.html',
            srcDir+'reservation.json',
            srcDir+'notifications.json',
            srcDir+'.htaccess'
        ],
        build: buildDir
    },
    browserSync: {
        baseDir: buildDir,
        files: [
            buildDir + 'js/*.*',
            buildDir + 'templates/**/**/*.*',
            buildDir + 'css/*.*'
        ],
        ignore: [
            'public/lib',
            'public/app',
            'public/js/**/*.min.js'
        ],
        open: false,
        port: 3001,
        reloadDelay: 1000
    }
};



/*********************************************************************************************************
 TASKS
 ********************************************************************************************************/

//Copy Config directory contents
gulp.task('config', function () {
    return gulp.src(config.config.src)
        .pipe(gulp.dest(config.config.build));
});

//Concatinate all js plugins
gulp.task('jsPlugins', function () {
    return gulp.src(config.jsPlugins.src)
    // .pipe(sourcemaps.init())
    //     .pipe(strip())
        .pipe(concat('plugins.js'))
        //only uglify if gulp is ran with '--type production'
        /*.pipe(gutil.env.type === 'production' ? uglify().on('error', function (err) {
            console.log(err.message);
        }) : gutil.noop())*/
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest(config.jsPlugins.build));
});


//Concatinate all angular components
gulp.task('js', function (){
    return gulp.src(config.js.src)
    // .pipe(sourcemaps.init())
       // .pipe(strip())
        .pipe(concat('app.js'))
        //only uglify if gulp is ran with '--type production'
        .pipe(gutil.env.type === 'production' ? uglify().on('error', function (err) {
            console.log(err.message);
        }) : gutil.noop())
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest(config.js.build));
});



//Concatinate all CSS Files
gulp.task('cssPlugins', function () {
    return gulp
        .src(config.cssPlugins.src)
        //.pipe(sourcemaps.init())
        //.pipe(sass( sassOptions ).on('error', sass.logError))
        // .pipe(sourcemaps.write())
        // .pipe(sass())
        .pipe(concat(config.cssPlugins.output))
        .pipe(gulp.dest(config.cssPlugins.css))
        //.pipe(minifyCSS())
        .pipe(gulp.dest(config.cssPlugins.build));
});



//Concatinate all CSS Files
gulp.task('css', function () {
    return gulp
        .src(config.css.src)
        //.pipe(sourcemaps.init())
        //.pipe(sass( sassOptions ).on('error', sass.logError))
        // .pipe(sourcemaps.write())
        // .pipe(sass())
        .pipe(concat(config.css.output))
        .pipe(gulp.dest(config.css.css))
        //.pipe(minifyCSS())
        .pipe(gulp.dest(config.css.build));
});

//Copy all images
gulp.task('images', function () {
    return gulp.src(config.images.src)
        .pipe(gulp.dest(config.images.build));
});


//Copy all Fonts
gulp.task('fonts', function () {
    return gulp.src(config.fonts.src)
        .pipe(gulp.dest(config.fonts.build));
});

//Copy all templates
gulp.task('templates', function () {
    return gulp.src(config.templates.src)
        .pipe(gulp.dest(config.templates.build));
});

//Copy external pages
gulp.task('copyPages', function () {
    return gulp.src(config.pages.src)
        .pipe(gulp.dest(config.pages.build));
});

// Copy Index.html and .htaccess
gulp.task('index', function () {
    return gulp.src(config.index.src)
        .pipe(gulp.dest(config.index.build));
});

gulp.task('cleanCssPlugins',['cssPlugins'], function () {
    return del([
        srcDir+'css/'+ config.cssPlugins.output
    ]);
});

gulp.task('cleanCss',['css'], function () {
    return del([
        srcDir+'css/'+ config.css.output
    ]);
});


//js lint task
gulp.task('jslint',['default'], function () {
    return gulp.src(['dist/app.js'])
        .pipe(jslint({ /* this object represents the JSLint directives being passed down */ }))
        .pipe(jslint.reporter('default'))
});

// Serve files over Node Server & Auto Sync Changes
gulp.task('serve', function() {
    bs.init({
        ui: { port: 8000 },
        port: config.browserSync.port,
        server: {
            baseDir: buildDir,
            middleware: [ historyApiFallback() ]
        },
        reloadDelay: config.browserSync.reloadDelay

    });
});

gulp.task('reload', function () {
    reload();
});


//Watch Task
gulp.task('watch', function () {
    gulp.watch(config.config.src, ['config', 'reload']);
    gulp.watch(config.jsPlugins.src, ['jsPlugins', 'reload']);
    gulp.watch(config.js.src, ['js', 'reload']);
    gulp.watch(config.cssPlugins.src, ['cssPlugins', 'reload']);
    gulp.watch(config.css.src, ['cleanCss', 'reload']);
    gulp.watch(config.images.src, ['images', 'reload']);
    gulp.watch(config.fonts.src, ['fonts', 'reload']);
    gulp.watch(config.templates.src, ['templates', 'reload']);
    gulp.watch(config.pages.src, ['copyPages', 'reload']);
    gulp.watch(config.index.src, ['index', 'reload']);
});

var uncss = require('gulp-uncss');
var nano = require('gulp-cssnano');
gulp.task('uncsss',['css','cssPlugins'], function () {
    return gulp.src(config.css.build)
        .pipe(uncss({
            html: ['index.html', 'src/templates/**/**/*.html', 'src/templates/**/**/**/*.html', 'src/templates/**/*.html']
        }))
        .pipe(nano())
        .pipe(gulp.dest('./out'));
});

/*********************************************************************************************************
 DEFAULT TASK
 ********************************************************************************************************/
gulp.task('default', [
    'config',           // Copy Config directory contents
    'jsPlugins',        // Concatenate all JS plugins
    'js',               // Concatenate JS Files
    'cleanCssPlugins',  // Concatenates all CSS plugins
    'cleanCss',         // Concatenates all custom CSS
    'images',           // Copy images
    'fonts',            // Copy fonts
    'templates',        // Copy templates
    'copyPages',        // Copy External Pages
    'index',            //Copy index.html and .htaccess
    'serve',            // Start Node Server
    'watch'             // Watch Files for Changes
]);