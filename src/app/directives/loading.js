Appx.directive('loading',['$window', function ($window) {
    return {
        restrict : 'AE',
        transclude : true,
        templateUrl : 'templates/shared/loadingindicator.html',
        scope : {
            show : '=',
            width : '@',
            height : '@',
            top : '@',
            left : '@'
        },
        link : function (scope) {
            var w = angular.element($window);
            scope.width = scope.width || 32;
            scope.height = scope.height || 32;
            scope.windowWidth = w.width();
            scope.windowHeight = w.height();
            scope.top =  scope.windowHeight/2;
            scope.left = scope.windowWidth/2;
            scope.$watch(function () {
                return {
                    'h': w.height(),
                    'w': w.width()
                };
            }, function (newValue) {
                scope.windowHeight = newValue.h;
                scope.windowWidth = newValue.w;
                scope.top =  scope.windowHeight/2;
                scope.left = scope.windowWidth/2;
            }, true);
            w.bind('resize', function () {
                scope.$apply();
            });
        }

    };
}]);