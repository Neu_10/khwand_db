// Handle Dropdown Hover Plugin Integration
Appx.directive('dropdownMenuHover', function () {
    return {
        link: function (scope, elem) {
            elem.dropdownHover();
        }
    };
});