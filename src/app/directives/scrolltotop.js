Appx.directive('scrolltotop',['$window', function ($window) {
    return {
        restrict : 'AE',
        transclude : true,
        link : function (scope) {
            var w = angular.element($window);
                var offset = 300;
                var duration = 500;

                if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {  // ios supported
                    $(window).bind("touchend touchcancel touchleave", function(e){
                        if ($(this).scrollTop() > offset) {
                            $('.scroll-to-top').fadeIn(duration);
                            $('.scroll-to-top').css('display','none');
                        } else {
                            $('.scroll-to-top').fadeOut(duration);
                            $('.scroll-to-top').css('display','block');
                        }
                    });
                } else {  // general
                    $(window).scroll(function() {
                        if ($(this).scrollTop() > offset) {
                            $('.scroll-to-top').fadeIn(duration);
                            $('.scroll-to-top').css('display','none');
                        } else {
                            $('.scroll-to-top').fadeOut(duration);
                            $('.scroll-to-top').css('display','block');
                        }
                    });
                }

                $('.scroll-to-top').click(function(e) {
                    e.preventDefault();
                    $('html, body').animate({scrollTop: 0}, duration);
                    return false;
                });


            w.bind('resize', function () {
                scope.$apply();
            });
        }

    };
}]);