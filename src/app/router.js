Appx.config([
    '$stateProvider',
    '$urlRouterProvider',
    '$locationProvider',
    function ($stateProvider, $urlRouterProvider, $locationProvider)
    {
        // Removing # from url
        $locationProvider.html5Mode({enabled: true, requireBase: true});

        // Redirect any unmatched url
        $urlRouterProvider.otherwise('/admin/');

        var APPX_TMPL_PATH = 'templates/admin/';
        var RESTAURANT_TMPL_PATH = 'templates/rest/';
        var SHARED_TMPL_PATH = 'templates/shared/';
        var PAGES_PATH = 'pages/';

        $stateProvider

        /*********************************************************************************************************

         STATIC PAGES

         ********************************************************************************************************/
            .state('static', {
                url: '/',
                abstract: true,
                templateUrl: SHARED_TMPL_PATH + 'pages/pages.html',
                controller: 'PagesController'
            })
            // TERMS
            .state('static.terms', {
                url: 'terms',
                data: {
                    pageTitle: 'Terms & Conditions',
                    iframeSrc: PAGES_PATH + 'terms.html'
                }
            })
            // PRIVACY
            .state('static.privacy', {
                url: 'privacy',
                data: {
                    pageTitle: 'Help',
                    iframeSrc: PAGES_PATH + 'privacy.html'
                }
            })

         /*********************************************************************************************************

         COMMON PAGES

         ********************************************************************************************************/
            
            // LOGIN
            .state('login', {
                url: '/login',
                templateUrl: SHARED_TMPL_PATH + 'login.html',
                controller: 'LoginController',
                data: { pageTitle: 'Login' }
            })

            // MASTER LOGIN
            .state('mlogin', {
                url: '/mlogin',
                templateUrl: SHARED_TMPL_PATH + 'masterlogin.html',
                controller: 'LoginController',
                data: { pageTitle: 'Login' }
            })

            /*********************************************************************************************************

             STEAMY ADMIN PAGES

             ********************************************************************************************************/

            // Admin Dashboard Controller
            .state('admin', {
                url: '/admin/',
                templateUrl: APPX_TMPL_PATH + 'main.html',
                data: {pageTitle: 'Dashboard'},
                abstract: true
            })

            // Dashboard first view
            .state('admin.main', {
                url: '',
                templateUrl: APPX_TMPL_PATH + 'dashboard/main.html',
                data: {pageTitle: 'Dashboard'},
                controller: 'AdminDashboardController'
            })

            // Dashboard first view
            .state('admin.orderHistory', {
                url: 'orderhistory',
                templateUrl: APPX_TMPL_PATH + 'orders/history.html',
                data: {pageTitle: 'Order history'}
            })


            // Dashboard first view
            .state('admin.orderDetails', {
                url: 'order/{orderId:int}',
                templateUrl: APPX_TMPL_PATH + 'orders/detail.html',
                data: { pageTitle: 'Orders | Detail' },
                controller: 'OrdersController'
            })

            // Profile
            .state('admin.profile', {
                url: 'profile',
                controller: 'AdminUserProfileController',
                templateUrl: APPX_TMPL_PATH + 'dashboard/profile.html',
                data: {pageTitle: 'Dashboard'}
            })


            //Restaurant Controller
            .state('admin.rest', {
                url: 'restaurants',
                templateUrl: APPX_TMPL_PATH + 'rest/main.html',
                abstract: true
            })
            // List all Restaurants
            .state('admin.rest.all', {
                url: '/all',
                templateUrl: APPX_TMPL_PATH + 'rest/all.html',
                data: { pageTitle: 'Restaurants'},
                controller: 'AllRestaurantsController'
            })
            //Add new restaurant
            .state('admin.rest.add', {
                url: '/add',
                templateUrl: APPX_TMPL_PATH + 'rest/add.html',
                data: {pageTitle: 'Add Restaurants'},
                controller: 'AllRestaurantsController'
            })
            //restaurant details by ID
            .state('admin.rest.profile', {
                url: '/restaurant/:restId',
                templateUrl: APPX_TMPL_PATH + 'rest/profile/main.html',
                data: { pageTitle: 'Restaurant Details' },
                abstract: true
            })

            .state('admin.rest.profile.overview', {
                url: '/overview',
                templateUrl: APPX_TMPL_PATH + 'rest/profile/overview.html',
                data: { pageTitle: 'Restaurant Details' },
                controller: 'RestaurantDetailsController'
            })

            .state('admin.rest.profile.basicinfo', {
                url: '/basicinfo',
                templateUrl: APPX_TMPL_PATH + 'rest/profile/basicinfo.html',
                data: { pageTitle: 'Restaurant Details' },
                controller: 'RestaurantBasicInfoController'
            })

            .state('admin.rest.profile.details', {
                url: '/details',
                templateUrl: APPX_TMPL_PATH + 'rest/profile/details.html',
                data: { pageTitle: 'Restaurant Details' },
                controller: 'RestaurantDetailsController'
            })

            .state('admin.rest.profile.timings', {
                url: '/timings',
                templateUrl: APPX_TMPL_PATH + 'rest/profile/timings.html',
                data: { pageTitle: 'Restaurant timings' },
                controller: 'RestaurantTimingsController'
            })

            .state('admin.rest.profile.pictures', {
                url: '/pictures',
                templateUrl: APPX_TMPL_PATH + 'rest/profile/pictures.html',
                data: { pageTitle: 'Restaurant pictures' },
                controller: 'RestaurantImagesController'
            })

            .state('admin.rest.profile.items', {
                url: '/items',
                templateUrl: APPX_TMPL_PATH + 'rest/profile/items.html',
                data: { pageTitle: 'Restaurant Details' },
                controller: 'ItemsController'
            })

            .state('admin.rest.profile.items.add', {
                url: '/add',
                templateUrl: APPX_TMPL_PATH + 'rest/profile/item-add.html',
                data: { pageTitle: 'Items | Add' }
            })

            .state('admin.rest.profile.categories', {
                url: '/categories',
                templateUrl: APPX_TMPL_PATH + 'rest/profile/categories.html',
                data: { pageTitle: 'Restaurant Details' },
                controller: 'ItemsController'
            })

            .state('admin.rest.profile.taxes', {
                url: '/taxes',
                templateUrl: APPX_TMPL_PATH + 'rest/profile/taxes.html',
                data: { pageTitle: 'Tax Details' },
                controller: 'ItemsController'
            })


            // USERS
            .state('admin.deliveryboys', {
                url: 'deliveryboys',
                templateUrl: APPX_TMPL_PATH + 'rest/profile/deliveryboys.html',
                data: {pageTitle: 'deliveryboys'},
                controller: 'DeliveryBoysController'
            })
            .state('admin.users.all', {
                url: '/all',
                templateUrl: APPX_TMPL_PATH + 'users/all.html',
                data: {pageTitle: 'All User'},
                controller: 'UserController'
            })
            .state('admin.users.detail', {
                url: '/profile',
                templateUrl: APPX_TMPL_PATH + 'users/profile.html',
                data: {pageTitle: 'User Profile'},
                controller: 'UserController'
            })

            // NOTIFICATIONS
            .state('admin.notifications', {
                url: 'notifications',
                templateUrl: APPX_TMPL_PATH + 'notifications.html',
                data: { pageTitle: 'Notifications' },
                controller: 'NotificationsController'
            })


            // 401 NOT FOUND
            .state('admin.unauthorized',{
                url: '401',
                templateUrl: SHARED_TMPL_PATH + '401.html',
                data: { pageTitle: 'Unauthorized' }
            })

            // 404 NOT FOUND
            .state('admin.notfound',{
                url: '404',
                templateUrl: SHARED_TMPL_PATH + '404.html',
                data: { pageTitle: 'Not Found' }
            })

            // 500 Server Error
            .state('admin.servererror',{
                url: '500',
                templateUrl: SHARED_TMPL_PATH + '500.html',
                data: { pageTitle: 'Server Error' }
            })

            // MAPS_TEST
            .state('admin.maps', {
                url: 'maps',
                templateUrl: APPX_TMPL_PATH + 'maps-test.html',
                data: {pageTitle: 'Mpas'},
                controller: 'MapsController',
               /* resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'Appx',
                            insertBefore: '#ng_load_plugins_before',
                            files: [
                                'js/lib/angular/angular-google-maps-street-view.min.js'
                            ]
                        });
                    }]
                }*/
            }

        );
    }
]);
