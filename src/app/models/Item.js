
Appx.service('Item', [
    'DataService',
    'HelperService',
    '$rootScope',
    '$q',
    'Restaurant',
    function (dataService,helperService, $rootScope, $q,restaurantModel)
    {
        var privates = {};
        privates.apiData = {};

        this.get = function (item) {
            return privates[item];
        };
        this.set = function (item, val) {
            privates[item] = val;
        };


        this.getItems = function () {
            var deferred = $q.defer();
            var url = restaurantModel.formatUrl($rootScope.urls.getItems),
                _self = this;
            dataService.get(url).then(function (data) {
                _self.set("itemsData", data.items);
                _self.set('itemObject',helperService.getObjFromArray(data.items));
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };


        this.getDashboardItems = function () {
            var deferred = $q.defer();
            var url = restaurantModel.formatUrl($rootScope.urls.getDashboardItems),
                _self = this;
            dataService.get(url).then(function (data) {
                _self.set("dashbroadItemsData", data.items);
                _self.set('dashboardItemObject',helperService.getObjFromArray(data.items));
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.addItem = function (itemDetails) {
            var deferred = $q.defer();
            var url = restaurantModel.formatUrl($rootScope.urls.addItems);
            dataService.post(url, itemDetails).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.updateItem = function (itemDetails) {
            var deferred = $q.defer();
            var url = restaurantModel.formatUrl($rootScope.urls.updateItem) + itemDetails.id;
            dataService.put(url, itemDetails).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.deleteItem = function (itemDetails) {
            var deferred = $q.defer();
            var url = restaurantModel.formatUrl($rootScope.urls.updateItem)+ itemDetails.id;
            dataService.delete(url).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.getItemNameForItemNumber = function (itemNumber) {
            var itemNameForNumber;
            itemNameForNumber = this.get('itemObject')[itemNumber];
            return itemNameForNumber ;
        };
    }

]);
