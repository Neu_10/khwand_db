
Appx.service('Delivery', [
    'DataService',
    'HelperService',
    '$rootScope',
    '$q',
    function (dataService, helperService,$rootScope, $q)
    {
        var privates = {};
        privates.apiData = {};

        this.get = function (item) {
            return privates[item];
        };
        this.set = function (item, val) {
            privates[item] = val;
        };

        var urls = $rootScope.urls;

        this.addDeliveryBoy = function(deliveryBoyDetails){
            var deferred = $q.defer(),
                url = urls.addDeliveryBoy;

            dataService.post(url,deliveryBoyDetails).then(function (data) {
                deferred.resolve(data);
            },function(err){
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.getDeliveryBoys = function () {
            var deferred = $q.defer(),
                url = urls.getDeliveryBoys;
            dataService.get(url).then(function(data){
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.getDeliveryBoyById = function (deliveryBoyId) {
            var deferred = $q.defer(),
                url = urls.getDeliveryBoys+'/deliveryBoyId';
            dataService.get(url).then(function(data){
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.setValue = function (keyString,value,arr) {
            var self =this;
            self.set(keyString,arr);
        };

        this.getTrackingCoords = function (orderId) {
            var data = 'https://mamelo.firebaseio.com/';
            //var data = url;
            return data;
        };

        this.assignDeliveryBoyToOrder = function (orderId) {
            var data = 'https://mamelo.firebaseio.com/';
            //var data = url;
            return data;
        };
        
    }
    
]);