
Appx.service('Order', [
    'DataService', 
    '$rootScope',
    '$q',
    function (dataService, $rootScope, $q)
    {
        var privates = {};
        privates.apiData = {};

        this.get = function (item) {
            return privates[item];
        };
        this.set = function (item, val) {
            privates[item] = val;
        };


        this.getAllOrders = function (pageNumber) {
            var deferred = $q.defer();
            var url = $rootScope.urls.getOrders+'?page='+pageNumber,
                _self = this;
            dataService.get(url).then(function (data) {
                _self.set("ordersData",data);
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.getDashboardOrders = function (pageNumber) {
            var deferred = $q.defer();
            var url = $rootScope.urls.getDashboardOrders+'?page='+pageNumber,
                _self = this;
            dataService.get(url).then(function (data) {
                _self.set("dashboardOrdersData",data);
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.getHistoryOrders = function (pageNumber,data) {
            var deferred = $q.defer();
            var url = $rootScope.urls.getHistoryOrders+'?page='+pageNumber,
                _self = this;
            dataService.post(url,data).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.getOrderById = function (orderId) {
            var deferred = $q.defer();
            var url = $rootScope.urls.getOrderById +orderId,
                _self = this;
            dataService.get(url).then(function (data) {
                _self.set("ordersData",data);
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        //add items to order
        this.addOrderItem = function (orderId, item) {
            var deferred = $q.defer();
            var url = $rootScope.urls.order+orderId;
            var _data = {
                "item_id": item.item_number.id,
                "qty": item.quantity
            };
            dataService.post(url, _data).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        //deletes items from order
        this.deleteOrderItem = function (orderId, itemId) {
            var deferred = $q.defer();
            var url = '/restaurant/order/'+orderId+'/item/'+ itemId;
            dataService.delete(url).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };


        //confirm unconfirmed items
        this.confirmAllItems = function (orderDetails) {
            var deferred = $q.defer();
            var url = $rootScope.urls.order+orderDetails.orderId;
            dataService.put(url,orderDetails).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };


        //cancel unconfirmed items
        this.cancelAllItems = function (orderId) {
            var deferred = $q.defer();
            var url = $rootScope.urls.order+orderId;
            dataService.delete(url).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };


        //Generate Bill
        this.generateBill = function(orderId){
            var deferred = $q.defer();
            var url = $rootScope.urls.order+orderId+'/generate_bill';
            dataService.post(url).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };




        this.updateQty = function (orderId, qty) {
            var deferred = $q.defer();
            var url = $rootScope.urls.order+'orderitem/'+orderId+'/'+qty;
            dataService.get(url).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };


        // CASH PAYMENT
        this.cashPay = function (data) {
            var deferred = $q.defer();
            var url = $rootScope.urls.receivepayment;
            dataService.post(url, data).then(function (data){
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.assignWaiter = function (data) {
            var deferred = $q.defer();
            var url = $rootScope.urls.assignWaiter;
            dataService.post(url, data).then(function (data){
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.assignTable = function (data) {
            var deferred = $q.defer();
            var url = $rootScope.urls.assignTable;
            dataService.post(url, data).then(function (data){
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.cancelOrder = function (oId) {
            var deffered = $q.defer();
            var url = $rootScope.urls.cancelOrder+oId;
            dataService.post(url,oId).then(function (data) {
                deffered.resolve(data);
            },function (err) {
               deffered.reject(err);
            });
            return deffered.promise;
        };

        this.getAvailableWaiters = function () {
            var deffered = $q.defer();
            var url = $rootScope.urls.getAvailableWaiters;
            dataService.get(url).then(function (data) {
                deffered.resolve(data);
            },function (err) {
               deffered.reject(err);
            });
            return deffered.promise;
        };

        /*this.filterOrderHistory = function (pageNum,data) {
            var deffered = $q.defer();
            var url = $rootScope.urls.getHistoryOrders;
            dataService.post(url,data).then(function (data) {
                deffered.resolve(data);
            },function (err) {
               deffered.reject(err);
            });
            return deffered.promise;
        };*/

    }
]);