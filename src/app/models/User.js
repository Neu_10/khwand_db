
Appx.service('User', [
    'DataService', 
    '$rootScope',
    '$q',
    function (dataService, $rootScope, $q) 
    {
        var privates = {};
        privates.apiData = {};

        this.get = function (item) {
            return privates[item];
        };
        this.set = function (item, val) {
            privates[item] = val;
        };

        var urls = $rootScope.urls;


        this.getAllUsers = function () {
            var deferred = $q.defer();
            var url = urls.getAllUsers;
            dataService.get(url).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.getUserInfo = function () {
            var deferred = $q.defer();
            var url = urls.userInfo;
            dataService.get(url).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

    }
]);
