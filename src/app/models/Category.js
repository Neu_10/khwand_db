
Appx.service('Category', [
    'DataService',
    '$rootScope',
    '$q',
    'Restaurant',
    function (dataService, $rootScope, $q,restaurantModel)
    {
        var privates = {};
        privates.apiData = {};

        this.get = function (item) {
            return privates[item];
        };
        this.set = function (item, val) {
            privates[item] = val;
        };

        var urls = $rootScope.urls;

        this.getAllCategories = function () {
            var deferred = $q.defer();
            var url = restaurantModel.formatUrl(urls.getAllCategories),
                _self = this;
            dataService.get(url).then(function (data) {
                _self.set("allCategoriesData",data);
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };
        this.getAllRestaurantCategories = function () {
            var deferred = $q.defer();
            var url = restaurantModel.formatUrl(urls.getRestaurantCategories),
                _self = this;
            dataService.get(url).then(function (data) {
                _self.set("restaurantCategoriesData",data);
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };
        this.addCategory = function (categoryDetails) {
            var deferred = $q.defer();
            var url = restaurantModel.formatUrl(urls.addCategory),
                _self = this;
            dataService.post(url,categoryDetails).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };
        this.deleteCategory = function (categoryId) {
            var deferred = $q.defer();
            var url = restaurantModel.formatUrl(urls.deleteCategory)+categoryId,
                _self = this;
            dataService.delete(url).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };
    }
]);
