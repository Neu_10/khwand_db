
Appx.service('Restaurant', [
    'DataService', 
    '$rootScope',
    '$q',
    '$stateParams',
    function (dataService, $rootScope, $q,$stateParams)
    {
        var privates = {};
        privates.apiData = {};

        this.get = function (item) {
            return privates[item];
        };
        this.set = function (item, val) {
            privates[item] = val;
        };


        this.getInit = function (pageNum) {
            var deferred = $q.defer();
            var _self = this;
            var url = $rootScope.urls.getInit+'?page='+pageNum;
            dataService.get(url).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.getAllRestaurants = function () {
            var deferred = $q.defer();
            var url = $rootScope.urls.getAllRestaurants;
            dataService.get(url).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };


        this.getRestaurantLocations = function () {
            var deferred = $q.defer();
            var url = $rootScope.urls.getRestaurantLocations;
            dataService.get(url).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };


        this.getRestaurantTypes = function () {
            var deferred = $q.defer();
            var url = $rootScope.urls.getRestaurantTypes;
            dataService.get(url).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.getRestaurantDetailsById = function (restaurantId) {
            var deferred = $q.defer();
            var _self = this;
            var url = $rootScope.urls.restaurantDetailsById + restaurantId;
            dataService.get(url).then(function (data) {
                _self.set('restaurantDetails', data);
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.addRestaurant = function (restaurantDetails) {
            var deferred = $q.defer();
            var _self = this;
            var url = $rootScope.urls.addRestaurant;
            dataService.post(url, restaurantDetails).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.addTable = function (tableDetails) {
            var deferred = $q.defer();
            var _self = this;
            var url = $rootScope.urls.addTable;
            dataService.post(url, tableDetails).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.addWaiter = function (waiterDetails) {
            var deferred = $q.defer();
            var _self = this;
            var url = $rootScope.urls.addAWaiter;
            dataService.post(url, waiterDetails).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.getTables = function () {
            var deferred = $q.defer();
            var _self = this;
            var url = $rootScope.urls.getTables;
            dataService.get(url).then(function (data) {
                _self.set('allTables',data);
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.getWaiters = function () {
            var deferred = $q.defer();
            var _self = this;
            var url = $rootScope.urls.getAllWaiters;
            dataService.get(url).then(function (data) {
                _self.set('allTables',data);
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.updateTableDetails = function (tableDetails) {
            var deferred = $q.defer();
            var _self = this;
            var url = $rootScope.urls.updateTableDetails+tableDetails.id;
            dataService.put(url, tableDetails).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.updateWaiterDetails = function (waiterDetails) {
            var deferred = $q.defer();
            var _self = this;
            var url = $rootScope.urls.updateWaiterDetails+waiterDetails.id;
            dataService.post(url, waiterDetails).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.deleteWaiter = function (waiter) {
            var deferred = $q.defer();
            var _self = this;
            var url = $rootScope.urls.deleteWaiter+waiter.id;
            dataService.delete(url).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.checkInCustomer = function (customerDetails) {
            var deferred = $q.defer();
            var _self = this;
            var url = $rootScope.urls.checkInCustomer;
            dataService.post(url,customerDetails).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.deleteTable = function (table) {
            var deferred = $q.defer();
            var _self = this;
            var url = $rootScope.urls.deleteTable+table.id;
            dataService.delete(url,table.id).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.changeItemStatus = function (itemStatusData) {
            var deferred = $q.defer();
            var _self = this;
            var url = 'restaurant/item/changestatus';
            var postData = {'status' : itemStatusData.status,'id' : itemStatusData.id};
            dataService.post(url,postData).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };
        
        this.search = function (data) {
            var deferred = $q.defer();
            var _self = this;
            var url = 'admin/professional/dbsearch?page=' + 1;
            dataService.post(url,data).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.getResultsPage = function (pageNumber) {
            var deferred = $q.defer();
            var _self = this;
            var url = 'admin/restaurants?page=' + pageNumber;
            dataService.get(url).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.restaurantStatusChange = function (stData) {
            var deferred = $q.defer();
            var _self = this;
            var url = 'admin/restaurant/changeStatus';
            dataService.post(url,stData).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };
        
        this.getRestaurantProfileDetails = function () {
            var deferred = $q.defer();
            var _self = this;
            var url = this.formatUrl($rootScope.urls.restProfile);
            dataService.get(url).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.saveTimeSlot = function (data) {
            var deferred = $q.defer();
            var _self = this;
            var url = $rootScope.urls.restTimingUpdate;
            var func = data.id !==  null ? dataService.put(url,data) : dataService.post(url,data);
            func.then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.removeTimeSlot = function (slotId) {
            var deferred = $q.defer();
            var _self = this;
            var url = $rootScope.urls.restTimingUpdate+'/'+slotId;
            dataService.delete(url).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };



        this.formatUrl = function (url) {
            var returnedUrl;
            var restId = this.get('selectedRestId') || $stateParams.restId;
            returnedUrl = url.replace(':id', restId);
            return returnedUrl;
        };
        
    }//function

]);