/**
 * Created by Nutan on 11/29/2016.
 */

Appx.service('TaxModel', [
    'DataService',
    '$rootScope',
    '$q',
    'Restaurant',
    function (dataService, $rootScope, $q,restaurantModel)
    {
        var privates = {};
        privates.apiData = {};

        this.get = function (item) {
            return privates[item];
        };
        this.set = function (item, val) {
            privates[item] = val;
        };

        var urls = $rootScope.urls;


        this.getTaxes = function () {
            var deferred = $q.defer();
            var url = restaurantModel.formatUrl($rootScope.urls.getTaxes);
            dataService.get(url).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.getTaxGroups = function () {
            var deferred = $q.defer();
            var url = restaurantModel.formatUrl($rootScope.urls.getTaxGroups);
            dataService.get(url).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.addTaxGroup = function (details) {
            var deferred = $q.defer();
            var url = restaurantModel.formatUrl($rootScope.urls.addTaxGroups);
            dataService.post(url,details).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.addTax = function (details) {
            var deferred = $q.defer();
            var url = restaurantModel.formatUrl($rootScope.urls.addTax);
            var _details = {};
            _details.data = details;
            dataService.post(url,details).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.updateTax = function (details) {
            var deferred = $q.defer();
            var url = restaurantModel.formatUrl($rootScope.urls.updateTax)+details.id;
            dataService.put(url,details).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.deleteTaxGroup = function (gId) {
            var deferred = $q.defer();
            var url = restaurantModel.formatUrl($rootScope.urls.deleteTaxGroups)+gId;
            dataService.delete(url).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.deleteTax = function (taxId) {
            var deferred = $q.defer();
            var url = restaurantModel.formatUrl($rootScope.urls.deleteTax)+taxId;
            dataService.delete(url).then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

    }
]);
