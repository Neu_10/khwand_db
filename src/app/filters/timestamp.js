Appx.filter('dateTimeFormat', function dateTimeFormat($filter){
    return function(text){
        if(text !== undefined){
            var  tempDate = new Date(text.replace(/-/g,"/"));
            return $filter('date')(tempDate, "dd/MM/yyyy h:mma");
        }
    }
});