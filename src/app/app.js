var Application = {};
Application.Factories = {};

Application.instance = angular.module('Appx', [
    'ui.router',
    'ui.bootstrap',
    'oc.lazyLoad',
    'ngSanitize',
    // 'ngJcrop',
    'angularUtils.directives.dirPagination',
    'ngStorage',
    'firebase',
    'xeditable',
    'datatables',
    'datatables.bootstrap',
    'ui.calendar',
    'ngTouch',
    'ngAnimate',
    'ng-backstretch',
    'ngFileUpload',
    'frapontillo.bootstrap-switch',
    'underscore',
    'checklist-model',
    'daterangepicker',
    'pascalprecht.translate',
    'ngMap',
    'ui.select'
]);

var Appx = Application.instance;

Appx.config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
}]);


/* Setup global settings */
Appx.factory('settings', ['$rootScope', function ($rootScope) {
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        layoutImgPath: Steamy.getAssetsPath() + 'admin/layout/img/',
        layoutCssPath: Steamy.getAssetsPath() + 'admin/layout/css/'
    };

    $rootScope.settings = settings;

    return settings;
}]);


/* Init global settings and run the app */
Appx.run([
    '$rootScope',
    'settings',
    '$state',
    '$http',
    '$localStorage',
    'editableOptions',
    '$location',
    'Auth',
    function ($rootScope, settings, $state, $http, $localStorage, editableOptions, $location, auth) {
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            $rootScope.loading = true;
            // console.log('toState '+toState.name + ' and From state '+fromState.name);
            //if token missing send to login page
            var autoLogin = function () {
                if ($localStorage.token == null) {
                    $location.path('/login');
                }
            };

            if (toState.name !== 'mlogin') {
                autoLogin();
            }


            var redirect = toState.redirectTo;
            if (redirect) {
                var user = auth.getUser($localStorage.token);
                $rootScope.userName = user.scope;
                if (angular.isString(redirect)) {
                    event.preventDefault();
                    $state.go(redirect, toParams);
                }
            }
        });

        $rootScope.$on('$stateChangeSuccess', function () {
            $rootScope.loading = false;
            window.scrollTo(0, 0);
        });

        $rootScope.$on('stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
            console.log('Transition failed with ' + error);
        });


        $rootScope.$on('$stateNotFound', function (event, unfoundState, fromState, fromParams) {
            console.log('state ' + unfoundState.to + ' has not been found');
            console.log(unfoundState.to); // "lazy.state"
            console.log(unfoundState.toParams); // {a:1, b:2}
            console.log(unfoundState.options); // {inherit:false} + default options
        });

        editableOptions.theme = 'bs3';
        $rootScope.$state = $state; // state to be accessed from view

        $http.get('config/config.json')
            .success(function (response) {
                $rootScope.baseURL = response.baseURL;
                $rootScope.baseDirectoryRef = response.baseDirectoryRef;
                $rootScope.countries = response.countries;
                $rootScope.users = response.users;
                $rootScope.urls = response.urls;
                $rootScope.config = response;
                $rootScope.isNotificationServiceEnabled = false;
            }).error(function () {
            console.log('config file missing');
        });
    }
]);


Appx.config(['$httpProvider', '$locationProvider', function ($httpProvider, $locationProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    // Uncomment this line to activate interceptor
    // $httpProvider.interceptors.push('AuthInterceptor');
    $locationProvider.html5Mode(true)
}
]);

Appx.config(['$translateProvider', function ($translateProvider) {

    $translateProvider.useStaticFilesLoader({
        prefix : 'config/i18n/',
        suffix : '.json'
    });
    $translateProvider.preferredLanguage('en-gb');
    $translateProvider.useSanitizeValueStrategy('escape');

}]);


