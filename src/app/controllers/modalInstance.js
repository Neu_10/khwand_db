/**
 * Created by Nutan on 9/20/2015.
 */


Appx.controller('ModalInstanceController', [
    '$scope',
    '$uibModalInstance',
    'item',
    function ($scope, $uibModalInstance, item) {
        if (item) {
            $scope.item = item;
            $scope.message = $scope.item.message;
            $scope.categories = $scope.item.categories; // added for categories in dropDown for edit Item
        }
        $scope.changedItem = {
            item: $scope.item
        };
        $scope.ok = function () {
            $uibModalInstance.close($scope.changedItem.item);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss();
        };
    }
]);

Appx.controller('TaxModalInstanceController', [
    '$scope',
    '$uibModalInstance',
    'item',
    function ($scope, $uibModalInstance, item) {
        if (item) {
            $scope.item = item;
            $scope.message = $scope.item.message;
            $scope.categories = $scope.item.categories; // added for categories in dropDown for edit Item
        }

        $scope.selectedTaxes = {
            taxes :  angular.copy($scope.item)
        };

        $scope.ok = function () {
            $uibModalInstance.close($scope.selectedTaxes.taxes);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss();
        };
    }
]);

Appx.controller('WaiterModalInstanceController', [
    '$scope',
    '$uibModalInstance',
    'item',
    function ($scope, $uibModalInstance, item) {
        if (item) {
            $scope.item = item;
            $scope.message = $scope.item.message;
            $scope.categories = $scope.item.categories;
        }

        $scope.ok = function (_item) {
            $uibModalInstance.close(_item);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss();
        };
    }
]);

Appx.controller('TableModalInstanceController', [
    '$scope',
    '$uibModalInstance',
    'tables',
    function ($scope, $uibModalInstance, tables) {
        if (tables) {
            $scope.tables = tables;
        }

        $scope.ok = function (_table) {
            $uibModalInstance.close(_table);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss();
        };
    }
]);