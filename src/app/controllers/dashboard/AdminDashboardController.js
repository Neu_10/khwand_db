'use strict';

Appx.controller('AdminDashboardController', [
    '$rootScope',
    '$scope',
    '$http',
    '$timeout',
    '$state',
    'Auth',
    '$location',
    'Restaurant',
    'User',
    'Order',
    'GeoCoder',
    function ($rootScope, $scope, $http, $timeout,$state, auth, $location, restaurantModel, userModel,ordersModel,geocoder)
    {
        $scope.loading = true;
        $scope.message = "Welcome to Steamy Admin Panel";
        var getAllOrders = function (pageNumber)
        {
            $scope.apiCallInError = false;
            ordersModel.getDashboardOrders(pageNumber).then(function (data)
            {
                $scope.loading = false;
                $scope.orders = data && data.orders ? data.orders.data : [];
                $scope.totalOrders = data && data.orders ? data.orders.order_count : 0;
                $scope.ordersPerPage = data && data.orders ? data.orders.per_page : 15;
                angular.forEach($scope.orders, function (order) {
                    if (order.ordertype != null) {
                        if (order.ordertype.toLowerCase() === 'dinein') {
                            order.orderTypeName = 'Dine In';
                            $scope.dineInOrders.unshift(order);
                        } else if (order.ordertype.toLowerCase() === 'home_delivery') {
                            order.orderTypeName = 'Home delivery';
                            $scope.deliveryOrders.unshift(order);
                        } else if (order.ordertype.toLowerCase() === 'preorder') {
                            order.orderTypeName = 'Pre-Order';
                            $scope.preOrders.unshift(order);
                        } else if (order.ordertype.toLowerCase() === 'pickup') {
                            order.orderTypeName = 'Pick-up';
                            $scope.pickUpOrders.unshift(order);
                        }
                    }
                });
            }, function (err) {
                $scope.loading = false;
                $scope.apiCallInError = true;
                $scope.noOrdersMessage = 'Could not get the orders. '+err.message;
                console.log('could not get all the orders');
            });
        };

        var getDeliveryBoys = function () {
            $scope.deliveryBoys = [];
        };

        getAllOrders(1);
        getDeliveryBoys(1);


    }
]);