

Appx.controller('TrackDeliveryController', [
    '$rootScope',
    '$scope',
    'settings',
    '$firebaseObject',
    '$localStorage',
    'Auth',
    'DeliveryModel',
    function ($rootScope, $scope, settings, $firebaseObject, $localStorage, auth, deliveryModel)
    {
        $scope.$on('$viewContentLoaded', function () {
            if ($localStorage.token) {
                var user = auth.getUser($localStorage.token);
                $rootScope.restaurantId = user.scope;
            }

            var trackOrder = function (keyToFindOrder) {
                var trackingURL = deliveryModel.getTrackingCoords(keyToFindOrder);
                var ref = new Firebase(trackingURL);
                console.log("firebase url" + trackingURL);
                $scope.data = {};
                $scope.coords = {};
                $scope.coordinates = [];
                $scope.obj = $firebaseObject(ref.child(keyToFindOrder));
            };


            trackOrder('0001'); //pass the key to track order/deliveryBoy

            // to take an action after the data loads, use the $loaded() promise
            $scope.obj.$loaded().then(function () {
                $scope.loading = false;
                $scope.obj.$bindTo($scope, "data").then(function () {
                    $scope.map = {
                        center: {
                            latitude: $scope.obj.latitude,
                            longitude: $scope.obj.longitude
                        }, zoom: 15
                    };
                    $scope.options = {scrollwheel: true};
                    $scope.marker = {
                        id: 0,
                        coords: {
                            latitude: $scope.obj.latitude,
                            longitude: $scope.obj.longitude
                        },
                        options: {draggable: false},
                        events: {
                            dragend: function (marker, eventName, args) {
                                $log.log('marker dragend');
                                var lat = marker.getPosition().lat();
                                var lon = marker.getPosition().lng();
                                $log.log(lat);
                                $log.log(lon);

                                $scope.marker.options = {
                                    draggable: true,
                                    labelContent: "lat: " + $scope.marker.coords.latitude + ' ' + 'lon: ' + $scope.marker.coords.longitude,
                                    labelAnchor: "100 0",
                                    labelClass: "marker-labels"
                                };
                            }
                        }
                    };
                    $scope.obj.$watch(function () {
                        console.log("value changed" + $scope.obj.latitude + " and " + $scope.obj.longitude);
                        $scope.marker = {
                            id: 0,
                            coords: {
                                latitude: $scope.obj.latitude,
                                longitude: $scope.obj.longitude
                            }
                        };
                        $scope.coords.latitude = $scope.obj.latitude;
                        $scope.coords.longitude = $scope.obj.longitude;

                        $scope.coordinates.push($scope.coords);
                        $scope.coords = {};

                    });


                });

            });
        });
    }]);
