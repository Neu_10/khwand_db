Appx.controller('DeliveryBoysController', [
    '$rootScope',
    '$scope',
    '$uibModal',
    'settings',
    'Restaurant',
    function ($rootScope, $scope, $modal, settings, restaurantModel) {
        $scope.$on('$viewContentLoaded', function () {
            $scope.message = "Add table via controller";
            $scope.alerts = [];
            $scope.loading = true;
            $scope.deliveryBoysPerPage = 25;
            $scope.pagination = {
                current : 1
            };

            //commenting below code as we are getting the tables on initial call only.
            var getAllWaiters = function () {
                restaurantModel.getWaiters().then(function (data) {
                    $scope.deliveryBoys = data.waiters;
                    $scope.loading = false;
                }, function (err) {
                    $scope.alerts.push({
                        type: 'danger',
                        msg: 'Sorry we are not able to get table information.Please try again.'
                    });
                });
            };

            getAllWaiters();

            $scope.addWaiter = function (waiterDetails) {
                $scope.loading = true;

                restaurantModel.addWaiter(waiterDetails).then(function (data) {
                    $scope.waiterDetails = {};
                    $scope.loading = false;
                    $scope.deliveryBoys.push(data.waiter);
                    $scope.alerts.push({type: 'success', msg: 'Waiter added successfully.'});
                }, function (err) {
                    $scope.loading = false;
                    $scope.alerts.push({type: 'danger', msg: 'Waiter addition failed.'});
                });
            };

            $scope.editWaiterClicked = function (waiter, status) {
                var _item = {},
                    _indexOfWaiter = $scope.deliveryBoys.indexOf(waiter);
                _item = angular.copy(waiter);
                if (status === 'statusChanged') {
                    restaurantModel.updateWaiterDetails(waiter).then(function (data) {
                        $scope.deliveryBoys[_indexOfWaiter] = waiter;
                        $scope.alerts.push({type: 'success', msg: 'deliveryBoys details changed successfully.'});
                    }, function (err) {

                    });
                } else {
                    var modalInstance = $modal.open({
                        templateUrl: 'templates/rest/waiters/edit-waiter.html',
                        controller: 'ModalInstanceController',
                        resolve: {
                            item: function () {
                                return _item;
                            }
                        }
                    });

                    // This resolves if the user accepts the modal prompt
                    modalInstance.result.then(function (returnedValueFromModal) {
                        restaurantModel.updateWaiterDetails(returnedValueFromModal).then(function (data) {
                            $scope.deliveryBoys[_indexOfWaiter] = returnedValueFromModal;
                            $scope.alerts.push({type: 'success', msg: 'deliveryBoys details changed successfully.'});
                        }, function (err) {

                        });
                        //write the logic after the item is edited.make a put call to the server.
                    });
                }
            };

            $scope.deleteWaiter = function (waiter) {
                var _item = {},
                    _indexOfWaiter = $scope.deliveryBoys.indexOf(waiter);
                _item = angular.copy(waiter);
                _item.message = 'You are going to remove waiter.This will remove the waiter from current assigned order.'
                    + 'Do you still want to delete?';
                //_table.categories = $scope.categories;
                var modalInstance = $modal.open({
                    templateUrl: 'templates/rest/modal-warning.html',
                    controller: 'ModalInstanceController',
                    resolve: {
                        item: function () {
                            return _item;
                        }
                    }
                });

                // This resolves if the user accepts the modal prompt
                modalInstance.result.then(function (returnedValueFromModal) {
                    restaurantModel.deleteWaiter(waiter).then(function () {
                        $scope.deliveryBoys.splice(_indexOfWaiter, 1);
                        $scope.alerts.push({type: 'success', msg: 'Waiter deleted successfully.'});
                    }, function (err) {

                    });
                    //write the logic after the item is edited.make a put call to the server.
                });
            };

            $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
            };

            $scope.resetDetails = function (waiterDetails) {
                $scope.waiterDetails = {};
            };

        });
    }
]);

