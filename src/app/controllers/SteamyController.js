Appx.controller('SteamyController', [
    '$rootScope',
    '$scope',
    function ($rootScope, $scope)
    {

        $scope.$on('$viewContentLoaded', function() {
            Steamy.init();
        });

        // Handler for messages coming from the service worker
        if('serviceWorker' in navigator){
            navigator.serviceWorker.addEventListener('message', function(event){
                $rootScope.$broadcast(event.data.tag, event.data);
            });
        }


    }
]);
