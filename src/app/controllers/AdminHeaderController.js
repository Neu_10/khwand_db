Appx.controller('AdminHeaderController', [
    '$rootScope',
    '$scope',
    'settings',
    'Auth',
    '$localStorage',
    '$http',
    function ($rootScope, $scope, settings, auth, $localStorage, $http)
    {
        $scope.logout = function () {
            auth.logout();
        };
        var displayName = $localStorage.displayName;

        /*if(displayName !== undefined ){
            $scope.displayName = displayName;
        } else {
            $scope.displayName = 'Administrator';
        }*/

        $scope.displayName = 'Administrator';

        // NOTIFICATIONS FUNCTIONS
        $http.get('notifications.json')
            .success(function (response)
            {
                $scope.notifications = response;
            }).error(function () {
            console.log('notifications file missing');
        });

    }

]);
