Appx.controller('LoginController', [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$localStorage',
    '$state',
    'Auth',
    function ($rootScope, $scope, $http, $location, $localStorage, $state, auth)
    {
        // Image Slide show on login page
         $scope.images = [
            "images/login/bg1.jpg",
            "images/login/bg2.jpg",
            "images/login/bg3.jpg"
        ];

        $http.get('config/config.json').success(function (response) {
            $scope.facebook_id = response.facebook_id;
            $scope.twitter_id = response.twitter_id;
            $scope.website = response.website;
        }).error(function () {
            console.log('error occured while getting config file');
        });

        $scope.mLogin = function (user) {
            auth.msignin(user).then(function (data) {
                if(data && data.response && data.response.error){
                    $scope.signinErrorMsg = data.response.message.mobile[0];
                    console.log($scope.errorMsg);
                } else if(data.status === '401'){
                    $scope.showUnauthorizedError = true;
                } else {
                    var user = {};
                    $localStorage.token = data.jwt;
                    user = auth.getUser($localStorage.token);
                    console.log(user);
                    if (user.role === "appx_admin") {
                        $state.go('admin.main');
                    }
                    else if (user.role === "rest_admin") {
                        $localStorage.displayName = data.rest.name;

                        // GO TO DASHBOARD
                        $state.go('rest.main');
                    }
                }
            })
        };

        $scope.showLoginForm =true;

        $scope.forgotPasswordBtnClicked = function(){
          $scope.showForgotPasswordForm = true;
          $scope.showLoginForm =false;
        };

        $scope.backBtnClicked = function(){
          $scope.showForgotPasswordForm = false;
          $scope.showLoginForm =true;
        };

        $scope.recoverPassword = function(recoveryEmail){
            auth.recoverPassword(recoveryEmail).then(function(response){
                console.log('Successfully sent mail '+response);
                $scope.showForgotPasswordForm = false;
                $scope.showLoginForm =false;
            },function(err){
              console.log('If any error e.g. email not regestered '+err);
            })
        };

        $scope.signin = function (user) {
           auth.signin(user).then(function (data) {
               if(data && data.response && data.response.error){
                   $scope.signinErrorMsg = data.response.message.mobile[0];
                   console.log($scope.errorMsg);
               }
               else if(data.status === '401'){
                   $scope.showUnauthorizedError = true;
               }else {
                   var user = {};
                   $localStorage.token = data.jwt;
                   user = auth.getUser($localStorage.token);
                   $localStorage.loggedInUser = data.user;
                   console.log(user);
                   if (user.role === "appx_admin") {
                       //Start Service worker
                       if ('serviceWorker' in navigator) {
                           //console.log('Service Worker is supported');
                           navigator.serviceWorker.register('sw.js').then(function() {
                               return navigator.serviceWorker.ready;
                           }).then(function(reg) {
                               //console.log('Service Worker is ready :^)', reg);
                               reg.pushManager.subscribe({
                                   userVisibleOnly: true
                               }).then(function(sub) {
                                   //console.log('endpoint:', sub.endpoint);
                                   // Post subscription info.
                                   auth.sendSUB(sub).then(function(){
                                       // console.log('subscription info sent successfully');
                                   })
                               });
                           }).catch(function(error) {
                               console.log('Service Worker error :^(', error);
                           });
                       }// End If
                       $state.go('admin.main');
                   }
                   else if (user.role === "rest_admin") {
                       $localStorage.displayName = data.rest.name;

                       $state.go('rest.main');
                   }
               }
           });
        };


        $scope.logout = function () {
            auth.logout().then(function (data) {
                $location.path('/');
            });
        };


        $scope.token = $localStorage.token;
        $scope.tokenClaims = auth.getTokenClaims();

    }
]);
