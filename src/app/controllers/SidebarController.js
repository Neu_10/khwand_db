
Appx.controller('SidebarController', [
    '$scope',
    '$rootScope',
    function ($scope, $rootScope)
    {

        $scope.$on('$includeContentLoaded', function() {
            Layout.initSidebar(); // init sidebar
        });

        $scope.toggleNotification = function () {
            $rootScope.isNotificationServiceEnabled = !$rootScope.isNotificationServiceEnabled;
        };
    }
]);
