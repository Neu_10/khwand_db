/**
 * Created by Nutan on 9/17/2015.
 */


/*THIS IS SAMPLE CODE TO BE USED IN EACH CONTROLLER WE WILL MAKE IN APPX.*/


/* Setup general page controller */
Appx.controller('MapsController', ['$rootScope',
    '$scope',
    'settings',
    'NgMap',
    'NavigatorGeolocation',
    '$firebaseObject',
    function($rootScope, $scope, settings,NgMap,NavigatorGeolocation,$firebaseObject) {
    $scope.$on('$viewContentLoaded', function() {
// initialize core components
        Steamy.initAjax();

// set default layout mode
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;

//START YOUR CODE AFTER THIS LINE -------- APPX ----
        NgMap.getMap('mymap').then(function(map) {
            console.log(map.getCenter());
            console.log('markers', map.markers);
            console.log('shapes', map.shapes);
        });




        NavigatorGeolocation.getCurrentPosition()
            .then(function(position) {
                var lat = position.coords.latitude, lng = position.coords.longitude;
              // do something lat and lng
            })
      var URL = "https://mamelo.firebaseio.com/";
        //   var URL = "https://nutan2.firebaseio.com/";
        // download the data into a local object
        var ref = new Firebase(URL);
        $scope.data = {};
        $scope.coords = {};
        $scope.coordinates = [];

        $scope.obj = $firebaseObject(ref.child('0001'));
               // to take an action after the data loads, use the $loaded() promise
        $scope.obj.$loaded().then(function() {
            $scope.obj.$bindTo($scope, "data").then(function() {
                $scope.map = {
                    center: {
                        latitude: $scope.obj.latitude,
                        longitude: $scope.obj.longitude
                    }, zoom: 15 };
                $scope.options = {scrollwheel: true};
                $scope.marker = {
                    id: 0,
                    coords: {
                        latitude: $scope.obj.latitude,
                        longitude: $scope.obj.longitude
                    },
                    options: { draggable: false },
                    events: {
                        dragend: function (marker, eventName, args) {
                            $log.log('marker dragend');
                            var lat = marker.getPosition().lat();
                            var lon = marker.getPosition().lng();
                            $log.log(lat);
                            $log.log(lon);

                            $scope.marker.options = {
                                draggable: true,
                                labelContent: "lat: " + $scope.marker.coords.latitude + ' ' + 'lon: ' + $scope.marker.coords.longitude,
                                labelAnchor: "100 0",
                                labelClass: "marker-labels"
                            };
                        }
                    }
                };
                $scope.obj.$watch(function () {
                    console.log("value changed"+$scope.obj.latitude+" and "+$scope.obj.longitude);
                    $scope.marker = {
                        id: 0,
                        coords: {
                            latitude: $scope.obj.latitude,
                            longitude: $scope.obj.longitude
                        }
                    };
                    $scope.coords.latitude = $scope.obj.latitude;
                    $scope.coords.longitude = $scope.obj.longitude;

                    $scope.coordinates.push($scope.coords);
                    $scope.coords = {};

                });





            });

        });


    });
}]);

