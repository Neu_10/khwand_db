
Appx.controller('CategoryController', [
    '$rootScope',
    '$scope',
    'settings',
    'Category',
    function ($rootScope, $scope, settings, categoryModel) 
    {
        $scope.$on('$viewContentLoaded', function () {
            $scope.categoryDetails = {};
            $scope.allCategories = [];
            $scope.restaurantCategories = [];
            var getAllRestaurantCategories = function () {
                categoryModel.getAllRestaurantCategories().then(function (data) {
                    $scope.restaurantCategories = data.itemCategories;
                }, function (err) {
                    $scope.showItemNotFetchedMessage = true;
                    $scope.itemNotFetchedMessage = 'Sorry, Something went wrong';
                });
            };

            getAllRestaurantCategories();
            $scope.addCategory = function (categoryDetails) {
                categoryModel.addCategory(categoryDetails).then(function (data) {
                    $scope.restaurantCategories.push(categoryDetails);
                    $scope.categoryDetails = {};
                    $scope.alerts.push({msg : 'Category Add succesfully',type : 'success'});
                }, function (err) {
                    $scope.alerts.push({msg : 'Category could not be added due to'+err,type : 'danger'});
                });
            };

            $scope.deleteCategory = function (categoryId) {
                categoryModel.deleteCategory(categoryId).then(function (data) {
                    console.log('Deletion successful');
                }, function (err) {
                    console.log('Error while removing category');
                });
            };

            $scope.closeAlert = function (indexOfAlert) {
                $scope.alerts.splice(indexOfAlert,1);
            }

        });
    }]);
