/**
 * AUTHORS:
 *      Ritesh Ranjan <ritesrnjn@gmail.com>
 *
 * Provides Data to Restaurant Controller
 */

Appx.controller('RestProfileController', [
    '$rootScope',
    '$scope',
    '$state',
    '$log',
    '$stateParams',
    '$localStorage',
    'Restaurant',
    function ($rootScope, $scope, $state, $log, $stateParams,$localStorage,restaurant)
    {
        $scope.loading = true;
        ComponentsPickers.init();

        function toDate(dStr,format) {
            var now = new Date();
            if (format == "HH:mm") {
                now.setHours(dStr.substr(0,dStr.indexOf(":")));
                now.setMinutes(dStr.substr(dStr.indexOf(":")+1));
                now.setSeconds(0);
                return now;
            }else
                return "Invalid Format";
        }

        function addZero(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }

        $scope.loggedInUser = $localStorage.loggedInUser;
        restaurant.getRestaurantProfileDetails().then(function (data) {
            $scope.loading = false;
            $scope.restaurantProfile = data.restaurant;
            $scope.restaurantDetails = data.restaurant.description;
            $scope.updateRestaurant = data.restaurant.description;
            $scope.facilities = data.restaurant.facilities;
            $scope.timings = data.restaurant.timing;
            $scope.closedDays = data.restaurant.timing.closed_on;
            $scope.updateRestaurantTimings = {
                mon_off : $scope.closedDays.indexOf('Monday')>-1,
                tue_off : $scope.closedDays.indexOf('Tuesday')>-1,
                wed_off : $scope.closedDays.indexOf('Wednesday')>-1,
                thu_off : $scope.closedDays.indexOf('Thursday')>-1,
                fri_off : $scope.closedDays.indexOf('Friday')>-1,
                sat_off : $scope.closedDays.indexOf('Saturday')>-1,
                sun_off : $scope.closedDays.indexOf('Sunday')>-1
            };
            angular.forEach($scope.timings.timeslot, function (slot) {
                slot.close = toDate(slot.close, 'HH:mm');
                slot.open = toDate(slot.open, 'HH:mm');
            });
        },function (err) {
            $log.error('error '+err);
        });

        $scope.dayTimings = function() {
            $scope.inserted = {
                id: null,
                day: $scope.days[0],
                open: toDate("00:00", 'HH:mm'),
                close:toDate("00:00", 'HH:mm')
            };
            $scope.timings && $scope.timings.timeslot ? $scope.timings.timeslot.push($scope.inserted) : '';
        };


        $scope.saveTimeSlot = function (slot,slotId) {
            slot.id=slotId;
            slot.open =  addZero(slot.open.getHours().toString()) +':'+addZero(slot.open.getMinutes().toString());
            slot.close =  addZero(slot.close.getHours().toString()) +':'+addZero(slot.close.getMinutes().toString());
            restaurant.saveTimeSlot(slot).then(function (data) {
                console.log('data '+data);
            })
        };

        $scope.removeTimeSlot = function (indexInSlots,slotId) {
            restaurant.removeTimeSlot(slotId).then(function (data) {
                $scope.timings && $scope.timings.timeslot ? $scope.timings.timeslot.splice($index,1) : '';
            });
        };

        $scope.days = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];

        $scope.uploadGalleryPics = function (files) {
            $scope.galleryPicFiles = files;
            var entityId = $scope.entity.description.id;

            if (files && files.length) {
                Upload.upload({
                    url: $rootScope.baseURL+'admin/entity/'+entityId+'/gallerypic',
                    data: {
                        files: files
                    }
                }).then(function (response) {
                    $timeout(function () {
                        $scope.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0) {
                        $scope.errorMsg = response.status;
                    }
                }, function (evt) {
                    $scope.galleryPicsProgress =
                        Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            }
        };

    }
]);
