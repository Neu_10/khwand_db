'use strict';

Appx.controller('AllRestaurantsController', [
    '$rootScope',
    '$scope',
    '$state',
    '$stateParams',
    'Restaurant',
    'NgMap',
    '$timeout',
    function ($rootScope, $scope, $state, $stateParams, restaurantModel,ngMap,$timeout)
    {
        $scope.restaurants = [];
        $scope.totalRestaurants = 0;
        $scope.RestaurantsPerPage = 0;
        $scope.pagination = {
            current: 1
        };

        function getResultsPage(pageNumber) {
            $scope.loading = true;
            restaurantModel.getResultsPage(pageNumber).then(function(data) {
                $scope.restaurants = data.restaurant.data;
                $scope.totalRestaurants = data.restaurant.total;
                $scope.RestaurantsPerPage = data.restaurant.per_page;
                $scope.loading = false;
            });
        }

        // Show All Restaurants list
        getResultsPage(1);

        $scope.search = function(data) {
            $scope.loading = true;
            restaurantModel.search(data).then(function (data){
                // dataService.post(url, _data).then(function (data){
                $scope.restaurants = data.restaurant.data;
                $scope.totalRestaurants = 25;
                $scope.RestaurantsPerPage = 25;
                $scope.loading = false;
            });
        };

        $scope.pageChanged = function(newPage) {
            getResultsPage(newPage);
        };

        $scope.restaurantStatusChange = function(statusData){
            $scope.loading = true;
            restaurantModel.restaurantStatusChange(statusData).then(function (data){
                console.log('status Changed '+data);
                $scope.loading = false;
            });
        };

        /*********************
         Add restaurant functions
        ************************/

        $scope.showRestaurantAdded = false;
        restaurantModel.getRestaurantLocations().then(function (data) {
            $scope.restaurantLocations = data.locations;
        });

        $scope.reloadRoute = function () {
            $state.go($state.current, {}, {reload: true});
        };

        restaurantModel.getRestaurantTypes().then(function (data) {
            $scope.restaurantTypes = data.restaurantTypes;
        });

        $scope.goToAddRestaurant = function () {
            $state.go('admin.rest.add');
        };

        $scope.resetAddRest = function (rest) {
            delete rest.engName;
            delete rest.pashtoName;
            delete rest.persianName;
            rest.name = '';
            rest.username = '';
            rest.addline1 = '';
            rest.addline2 = '';
            rest.addline3 = '';
            $scope.showPashtoNameField = false;
            $scope.showPersianNameField = false;
        };

/***
 * Map related functions
 * ***/

        $scope.setMarker = function (arg) {
            var place = this.getPlace();
            $scope.map.setCenter(place.geometry.location);
            $scope.map.markers[0].setPosition(place.geometry.location);
        };

        ngMap.getMap().then(function(map) {
            $scope.map = map;
        });

        $scope.getCurrentLocation = function(event,rest){
            rest.lat = event.latLng.lat();
            rest.lng = event.latLng.lng();
            var _geocoder = new google.maps.Geocoder();
            var latlng =  new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());
            _geocoder.geocode({ 'latLng': latlng }, function(results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        console.log(results[1]);
                        rest.addline1 = results[1].formatted_address;
                        console.log(rest.addline1);
                        $scope.$apply();
                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });
        };
        /***
         * Map related functions end
         * ***/
        $scope.addNewRest = function(rest) {
            $scope.loading = true;
            rest.name = {};
            rest.name.en = angular.copy(rest.engName);
            rest.name.ps = angular.copy(rest.pashtoName);
            rest.name.fa = angular.copy(rest.persianName);
            restaurantModel.addRestaurant(rest)
                .then(function (data) {
                    $scope.loading = false;
                    $scope.showRestaurantAdded = true;
                    $timeout(function () {
                        $scope.showRestaurantAdded = false;
                    }, 3000);
                    if(!data.error){
                        $scope.resetAddRest(rest);
                        $scope.restaurantAddedMsg = data.message;
                        $scope.inError = false;
                    }else{
                        $scope.restaurantAddedMsg = data.message;
                        $scope.inError = true;
                    }
                }, function (err) {
                    $scope.loading = false;
                    $scope.restaurantAddedMsg = err.message;
                    $scope.inError = true;
                });
        };

        $scope.addInPashto = function () {
            //duplicate
            $scope.showPashtoNameField = true;

        };

        $scope.addInPersian = function () {
            //duplicate
            $scope.showPersianNameField = true;
        };



    }
]);
