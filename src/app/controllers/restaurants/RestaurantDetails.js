
Appx.controller('RestaurantDetailsController', [
    '$rootScope',
    '$scope',
    '$http',
    '$timeout',
    'Auth',
    '$location',
    '$state',
    '$stateParams',
    'Restaurant',
    '$firebaseObject',
    '$localStorage',
    '$log',
    'DataService',
    function ($rootScope, $scope, $http, $timeout, auth, $location, $state, $stateParams, restaurantModel, $firebaseObject,$localStorage,$log,dataService)
    {
        var viewRestaurantDetailsById = function (restaurantId) {
            $scope.loading = true;
            restaurantModel.getRestaurantDetailsById(restaurantId).then(function (data) {
                $scope.loading = false;
                $scope.restaurantDetails = data;
                restaurantModel.set("selectedRestaurantDetails",data);
                restaurantModel.set('selectedRestId',restaurantId);
                $scope.restaurantDetails = restaurantModel.get('selectedRestaurantDetails');
                $scope.restaurant =$scope.restaurantDetails.restaurant ;
                $scope.updateRestaurant = data.restaurant;
            }, function (err) {
                console.log('Inside error of restaurantDetails controller');
            });
        };

        var restaurantId = $stateParams.restId;

        // Show restaurant details ---- Overview section

        viewRestaurantDetailsById(restaurantId);

        function toDate(dStr,format) {
            var now = new Date();
            if (format == "HH:mm") {
                now.setHours(dStr.substr(0,dStr.indexOf(":")));
                now.setMinutes(dStr.substr(dStr.indexOf(":")+1));
                now.setSeconds(0);
                return now;
            }else
                return "Invalid Format";
        }

        function addZero(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }

        restaurantModel.getRestaurantProfileDetails().then(function (data) {
            $scope.restaurantProfile = data.restaurant;
            $scope.restaurantDetails = data.restaurant.description;
            $scope.updateRestaurant = data.restaurant.description;
            $scope.facilities = data.restaurant.facilities;
            $scope.timings = data.restaurant.timing;
            $scope.closedDays = data.restaurant.timing.closed_on;
            $scope.updateRestaurantTimings = {
                mon_off : $scope.closedDays.indexOf('Monday')>-1,
                tue_off : $scope.closedDays.indexOf('Tuesday')>-1,
                wed_off : $scope.closedDays.indexOf('Wednesday')>-1,
                thu_off : $scope.closedDays.indexOf('Thursday')>-1,
                fri_off : $scope.closedDays.indexOf('Friday')>-1,
                sat_off : $scope.closedDays.indexOf('Saturday')>-1,
                sun_off : $scope.closedDays.indexOf('Sunday')>-1
            };
            angular.forEach($scope.timings.timeslot, function (slot) {
                slot.close = toDate(slot.close, 'HH:mm');
                slot.open = toDate(slot.open, 'HH:mm');
            });
        },function (err) {
            $log.error('error '+err);
        });



        // Basic info
        //Amenities
        //Timings
        //Pictures

        $scope.updateRestaurantBasicInfo = function (restDetails) {
            var url = $rootScope.urls.updateRestBasicInfo;
            dataService.post(url, restDetails).then(function (data) {
                console.log("updated successfully");
            }, function (err) {
                console.log(" update error "+err);
            });
        };

        $scope.uploadProfilePic = function (dataUrl) {
            Upload.upload({
                url: $rootScope.baseURL+'admin/professional/'+professionalId+'/profilepic',
                data: {
                    profile_pic: Upload.dataUrltoBlob(dataUrl)
                }
            }).then(function (response) {
                $timeout(function () {
                    $scope.result = response.data;
                });
            }, function (response) {
                if (response.status > 0) $scope.errorMsg = response.status
                    + ': ' + response.data;
            }, function (evt) {
                $scope.proProfileProgress = Math.min(parseInt(100.0 * evt.loaded / evt.total));
            });
        };

    }

]);

