Appx.controller('ItemsController', [
    '$rootScope',
    '$scope',
    '$timeout',
    '$state',
    '$uibModal',
    'settings',
    'DTOptionsBuilder',
    'Item',
    'Category',
    'TaxModel',
    function ($rootScope, $scope, $timeout, $state, $modal, settings, dTOptionsBuilder, itemModel, categoryModel,taxModel)
    {
        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = true;
            $scope.showAddItemDiv = false;
            $scope.showCategoryAdded = false;
            $scope.showItemAdded = false;
            //Items
            //Categories
            //Taxes


            //START YOUR CODE AFTER THIS LINE -------- APPX ----
            $scope.itemDetails = {};
            $scope.itemDetails.taxgroup = {};
            var getItems = function () {
                itemModel.getItems().then(function (data) {
                    $scope.items = data.items;
                    $scope.loading = false;
                }, function (err) {
                    $scope.loading = false;
                    $scope.showItemNotFetchedMessage = true;
                    $scope.itemNotFetchedMessage = 'Sorry, Something went wrong.'+err.message;
                });
            };

            var getAllCategories = function () {
                categoryModel.getAllCategories().then(function (data) {
                    $scope.allCategories = data.itemCategories;
                }, function (err) {
                    $scope.showItemNotFetchedMessage = true;
                    $scope.itemNotFetchedMessage = 'Sorry, Something went wrong';
                });
            };

            getAllCategories();
            getItems();

            $scope.toggleAllAndAddItems = function () {
              $scope.showAddItemDiv = !$scope.showAddItemDiv;
            };

            $scope.toggleAllAndAddCategory = function () {
              $scope.showAddCategoryDiv = !$scope.showAddCategoryDiv;
            };

            $scope.options = [{"name" : "Yes"},{"name":"No"}];
           // $scope.options = ["Yes","No"];

// Add Items

            $scope.addItemInPashto = function () {
                //duplicate
                $scope.showPashtoItemNameField = true;

            };

            $scope.addItemInPersian = function () {
                //duplicate
                $scope.showPersianItemNameField = true;
            };
            $scope.addItemDescInPashto = function () {
                //duplicate
                $scope.showPashtoItemDescField = true;

            };

            $scope.addItemDescInPersian = function () {
                //duplicate
                $scope.showPersianItemDescField = true;
            };

            $scope.addItem = function (itemDetails) {
                $scope.itemAdditionFailed = false;
                itemDetails.name = {};
                itemDetails.name.en = angular.copy(itemDetails.engName);
                itemDetails.name.ps = angular.copy(itemDetails.pashtoName);
                itemDetails.name.fa = angular.copy(itemDetails.persianName);
                itemDetails.description = {};
                itemDetails.description.en = angular.copy(itemDetails.engDesc);
                itemDetails.description.ps = angular.copy(itemDetails.pashtoDesc);
                itemDetails.description.fa = angular.copy(itemDetails.persianDesc);
                $scope.loading = true;
                var _itemDetails = itemDetails;
                _itemDetails.save = 'addItem';
                _itemDetails.taxgroup_id = _itemDetails.taxgroup.id;
              itemModel.addItem(_itemDetails).then(function (data) {
                  if(data.response && data.response.error){
                      $scope.alerts.push({type: 'danger', msg: data.response.message});
                  }else{
                      delete itemDetails.engDesc;
                      delete itemDetails.pashtoDesc;
                      delete itemDetails .persianDesc;
                      delete itemDetails.engName;
                      delete itemDetails.pashtoName;
                      delete itemDetails .persianName;
                      $scope.items.push(itemDetails);
                      $scope.alerts.push({type: 'success', msg: 'Details added successfully.'});
                  }
                  $scope.loading = false;
              }, function () {
                  $scope.itemAdditionFailed = true;
                  $scope.alerts.push({type: 'danger', msg: 'Sorry,something went wrong.Item could not be created.Please try again'});
                  console.log("item addition failed ");
                  $scope.loading = false;
                  $state.go($state.current, {}, {reload: true});
              });
            };

            // Add Items end

            $scope.deleteItemClicked = function(item) {
                var _item = {};
                _item = item;
                var _indexOfItem = $scope.items.indexOf(item);
                _item.message  = 'You are about to delete '+_item.name+' from items.Do you want to remove this from item list?';
                var modalInstance = $modal.open({
                    templateUrl: 'templates/admin/rest/modal-warning.html',
                    controller: 'ModalInstanceController',
                    resolve: {
                        item: function() {
                            return _item;
                        }
                    }
                });

                // This resolves if the user accepts the modal prompt
                modalInstance.result.then(function(item) {
                    $scope.loading = true;
                    itemModel.deleteItem(item).then(function () {
                        $scope.items.splice(_indexOfItem,1);
                        $scope.loading = false;
                        $scope.alerts.push({type: 'success', msg: 'Item deleted successfully.'});
                    }, function (err) {
                        $scope.loading = false;
                        $scope.alerts.push({type: 'danger', msg: 'Sorry,something went wrong.Item couldn\'t be deleted'});
                    });
                });
            };
            $scope.editItemClicked = function(item) {
                var _item = {};
                _item = item;
                _item.categories = $scope.allCategories;
                _item.taxgroups = $scope.taxGroups;
                var modalInstance = $modal.open({
                    templateUrl: 'templates/admin/rest/profile/item-edit.html',
                    controller: 'ModalInstanceController',
                    resolve: {
                        item: function() {
                            return _item;
                        }
                    }
                });

                // This resolves if the user accepts the modal prompt
                modalInstance.result.then(function(item) {
                    $scope.loading = true;
                    //write the logic after the item is edited.make a put call to the server.
                    item.taxgroup_id = item.taxgroup.id;
                    itemModel.updateItem(item).then(function () {
                    }, function (err) {

                    });
                });
            };

            //////////////////////////////////////////////////
            ///------------------Taxes---------------------///
            //////////////////////////////////////////////////

            $scope.taxes = [];
            $scope.message = "Taxes view";
            $scope.alerts = [];
            $scope.taxDetails={};
            $scope.taxGroupDetails = {};
            $scope.taxGroupDetails.selectedTaxes = [];
            $scope.isTakingTax = true;

            $scope.toggleTax = function (status) {
                $scope.isTakingTax = status;
            };

            $scope.addTaxRow = function () {

            };

            $scope.editTaxGroupDetails = function (_taxGroupDetails) {
                _taxGroupDetails.taxes && _taxGroupDetails.taxes.length> 0 ? _taxGroupDetails.selectedTaxes = angular.copy(_taxGroupDetails.taxes) : '';
                $scope.taxGroupDetails = _taxGroupDetails;
            };

            $scope.editTaxDetails = function (taxDetails) {
                taxModel.updateTax(taxDetails).then(function (data) {
                    $scope.taxes = data.taxes;
                    console.log('got taxes data '+data);
                },function (err) {
                    console.log('error '+err);
                });
            };


            $scope.getSelectedItemIndex = function (indexOfItem) {
                console.log('esda 0'+indexOfItem);
            };

            $scope.getAllTaxes = function () {
                taxModel.getTaxes().then(function (data) {
                    $scope.taxes = data.taxes;
                    $scope.loading = false;
                    console.log('got taxes data '+data);
                },function (err) {
                    console.log('error '+err);
                });
            };

            $scope.getAllTaxGroups = function () {
                taxModel.getTaxGroups().then(function (data) {
                    $scope.taxGroups = data.taxgroup;
                    console.log('got taxes data '+data);
                },function (err) {
                    console.log('error '+err);
                });
            };

            $scope.getAllTaxes();
            $scope.getAllTaxGroups();

            $scope.addTaxGroup = function (details) {
                details.taxes = [];
                details.name = {};
                details.name.en = angular.copy(details.engName);
                details.name.ps = angular.copy(details.pashtoName);
                details.name.fa = angular.copy(details.persianName);
                details.description = {};
                details.description.en = angular.copy(details.engDesc);
                details.description.ps = angular.copy(details.pashtoDesc);
                details.description.fa = angular.copy(details.persianDesc);
                angular.forEach(details.selectedTaxes,function (tax) {
                    details.taxes.push(tax.id);
                });
                taxModel.addTaxGroup(details).then(function (data) {
                    details = {};
                    $scope.taxGroups.push(data.taxgroup);
                },function (err) {
                    console.log('error '+err);
                })
            };


            $scope.deleteTaxGroup = function (details) {
                var _indexOfGroup = $scope.taxGroups.indexOf(details);
                taxModel.deleteTaxGroup(details.id).then(function (data) {
                    details = {};
                    $scope.taxGroups.splice(_indexOfGroup,1);
                },function (err) {
                    console.log('error '+err);
                })
            };
            $scope.deleteTax = function (details) {
                var _tax = {};
                _tax = details;
                var _indexOfTax = $scope.taxes.indexOf(details);
                _tax.message  = 'You are about to delete '+_tax.name+' from taxes.Do you want to remove this from tax list?';
                var modalInstance = $modal.open({
                    templateUrl: 'templates/admin/rest/modal-warning.html',
                    controller: 'ModalInstanceController',
                    resolve: {
                        item: function() {
                            return _tax;
                        }
                    }
                });

                // This resolves if the user accepts the modal prompt
                modalInstance.result.then(function(item) {
                    $scope.loading = true;
                    taxModel.deleteTax(item.id).then(function (data) {
                        details = {};
                        $scope.loading = false;
                        $scope.taxes.splice(_indexOfTax,1);
                        $scope.alerts.push({type: 'success', msg: 'Item deleted successfully.'});
                    },function (err) {
                        console.log('error '+err);
                        $scope.loading = false;
                        $scope.alerts.push({type: 'danger', msg: 'Sorry,something went wrong.Item couldn\'t be deleted'});
                    });
                });
            };
            $scope.showPashtoTaxNameField = false;
            $scope.showPersianTaxNameField = false;
            $scope.showPashtoTaxGroupNameField = false;
            $scope.showPersianTaxGroupNameField = false;
            $scope.showPashtoTaxGroupDescField = false;
            $scope.showPersianTaxGroupDescField = false;

            $scope.addTaxNameInPashto = function () {
                $scope.showPashtoTaxNameField = true;
            };

            $scope.addTaxNameInPersian = function () {
                $scope.showPersianTaxNameField = true;
            };

            $scope.addTaxGroupNameInPashto = function () {
                $scope.showPashtoTaxGroupNameField = true;
            };

            $scope.addTaxGroupNameInPersian = function () {
                $scope.showPersianTaxGroupNameField = true;
            };

            $scope.addTaxGroupDescInPashto = function () {
                $scope.showPashtoTaxGroupDescField = true;
            };

            $scope.addTaxGroupDescInPersian = function () {
                $scope.showPersianTaxGroupDescField = true;
            };

            $scope.addTax = function (details) {
                console.log('details '+details);
                details.name = {};
                details.name.en = angular.copy(details.engName);
                details.name.ps = angular.copy(details.pashtoName);
                details.name.fa = angular.copy(details.persianName);
                var tempTaxLevel = details.taxLevel+1;
                  $scope.taxLevels.indexOf(details.taxLevel) === ($scope.taxLevels.length-1) ?
                 $scope.taxLevels.push(tempTaxLevel) : '';
                 $scope.taxes.push(details); // remove this line once API is integrated
                 ComponentsBootstrapSelect.init();
                 $scope.taxDetails = {};
                 $scope.taxDetails.taxLevel = $scope.taxLevels[0];
                taxModel.addTax(details).then(function (data) {

                },function (err) {
                    console.log('could not add tax '+err);
                })
            };

            $scope.taxLevels = [0];
            $scope.taxDetails.taxLevel = $scope.taxLevels[0];
            $scope.levelHelpText = !$scope.showTaxLevelInfo ? 'Hide tax level instructions' : 'View tax level instructions';
            $scope.showTaxLevelInfo = false;
            $scope.toggleHelpText = function () {
                $scope.showTaxLevelInfo = !$scope.showTaxLevelInfo;
                $scope.levelHelpText = !$scope.showTaxLevelInfo ? 'Hide tax level instructions' : 'View tax level instructions';
            };


            $scope.addTaxesToGroup = function(item) {
                var _item = item;
                var modalInstance = $modal.open({
                    templateUrl: 'templates/rest/taxes/select-taxes.html',
                    controller: 'TaxModalInstanceController',
                    resolve: {
                        item: function() {
                            return _item;
                        }
                    }
                });

                // This resolves if the user accepts the modal prompt
                modalInstance.result.then(function(item) {
                    $scope.loading = true;
                    //write the logic after the item is edited.make a put call to the server.
                    $scope.addedTaxesInGroup = item;
                    $scope.showTaxesAddedBox = true;
                    console.log('sdd '+item);
                });
            };

            ///////////////////cat

            $scope.categoryDetails = {};
            $scope.allCategories = [];
            $scope.restaurantCategories = [];
            var getAllRestaurantCategories = function () {
                categoryModel.getAllRestaurantCategories().then(function (data) {
                    $scope.restaurantCategories = data.itemCategories;
                }, function (err) {
                    $scope.showItemNotFetchedMessage = true;
                    $scope.itemNotFetchedMessage = 'Sorry, Something went wrong';
                });
            };

            getAllRestaurantCategories();
            $scope.addCategory = function (categoryDetails) {
                categoryDetails.name = {};
                categoryDetails.name.en = angular.copy(categoryDetails.engName);
                categoryDetails.name.ps = angular.copy(categoryDetails.pashtoName);
                categoryDetails.name.fa = angular.copy(categoryDetails.persianName);
                categoryDetails.description = {};
                categoryDetails.description.en = angular.copy(categoryDetails.engDesc);
                categoryDetails.description.ps = angular.copy(categoryDetails.pashtoDesc);
                categoryDetails.description.fa = angular.copy(categoryDetails.persianDesc);
                delete categoryDetails.engName;
                delete categoryDetails.pashtoName;
                delete categoryDetails .persianName;
                delete categoryDetails.engDesc;
                delete categoryDetails.pashtoDesc;
                delete categoryDetails .persianDesc;
                categoryModel.addCategory(categoryDetails).then(function (data) {
                    $scope.showCategoryAdded = true;
                    $timeout(function () {
                        $scope.showCategoryAdded = false;
                    }, 10000);
                    $scope.inError = false;
                    $scope.restaurantCategories.push(categoryDetails);
                    $scope.categoryDetails = {};
                    $scope.alerts.push({msg : 'Category Added succesfully',type : 'success'});
                }, function (err) {
                    $scope.inError = true;
                    $scope.showCategoryAdded = true;
                    $timeout(function () {
                        $scope.showCategoryAdded = false;
                    }, 10000);
                    $scope.alerts.push({msg : 'Category could not be added due to an error occured',type : 'danger'});
                });
            };

            $scope.editCategory = function(item) {
                var _category = {};
                _category = category;
                _category.categories = $scope.allCategories;
                _category.taxgroups = $scope.taxGroups;
                var modalInstance = $modal.open({
                    templateUrl: 'templates/admin/rest/profile/category-edit.html',
                    controller: 'ModalInstanceController',
                    resolve: {
                        item: function() {
                            return _category;
                        }
                    }
                });

                // This resolves if the user accepts the modal prompt
                modalInstance.result.then(function(item) {
                    $scope.loading = true;
                    //write the logic after the item is edited.make a put call to the server.
                    item.taxgroup_id = item.taxgroup.id;
                    itemModel.updateItem(item).then(function () {
                    }, function (err) {

                    });
                });
            };

            $scope.deleteCategory = function (category) {
                var indexOfCat = $scope.restaurantCategories.indexOf(category);
                categoryModel.deleteCategory(category.id).then(function (data) {
                    console.log('Deletion successful');
                    $scope.alerts.push({type: 'success', msg: 'Category deleted successfully.'});
                    $scope.restaurantCategories.splice(indexOfCat,1);
                }, function (err) {
                    $scope.alerts.push({type: 'danger', msg: 'Sorry . An error occured while deleting category.'});
                });
            };

            $scope.closeAlert = function (indexOfAlert) {
                $scope.alerts.splice(indexOfAlert,1);
            };

            $scope.showPashtoCategoryNameField = false;
            $scope.showPersianCategoryNameField = false;
            $scope.showPersianCategoryDescField = false;
            $scope.showPersianCategoryDescField = false;

            $scope.addCategoryInPashto = function () {
                //duplicate
                $scope.showPashtoCategoryNameField = true;
            };

            $scope.addCategoryInPersian = function () {
                //duplicate
                $scope.showPersianCategoryNameField = true;
            };

            $scope.addCategoryDescInPersian = function () {
                //duplicate
                $scope.showPersianCategoryDescField = true;

            };

            $scope.addCategoryDescInPashto = function (el) {
                //duplicate
                $scope.showPashtoCategoryDescField = true;
            };


        });
    }]);
