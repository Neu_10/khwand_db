Appx.service('Auth',[
    '$localStorage',
    'DataService',
    '$state',
    '$q',
    '$location',
    '$rootScope',
    function ($localStorage,dataService,$state,$q,$location,$rootScope)
    {
        var privates = {};
        privates.apiData = {};

        this.get = function (item) {
            return privates[item];
        };
        this.set = function (item, val) {
            privates[item] = val;
        };

        this.urlBase64Decode = function (str) {
            var output = str.replace('-', '+').replace('_', '/');
            switch (output.length % 4) {
                case 0:
                    break;
                case 2:
                    output += '==';
                    break;
                case 3:
                    output += '=';
                    break;
                default:
                    throw 'Illegal base64url string!';
            }
            return window.atob(output)
        };
        

        //matches token with one stored in localstorage
        this.matchToken = function (token) {
            return token == $localStorage.token;
        };

        this.signup = function (data) {
         var url = $rootScope.urls.signup,
             _data = data;
            dataService.post(url, _data).then(function (data) {
                console.log("signup successful. token is "+response);
            }, function (err) {
               console.log("signup failed");
            });
        };

        this.signin = function (data) {
            var deferred = $q.defer();
            var  url = $rootScope.urls.signin,
                _self = this;
             dataService.post(url,data).then(function(data) {
                deferred.resolve(data);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        // Master Signin
        this.msignin = function (data) {
            var deferred = $q.defer();
            var  url = $rootScope.urls.msignin;
            dataService.post(url,data).then(function(data) {
                deferred.resolve(data);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        this.logout = function () {
            var tokenClaims = {};
            delete $localStorage.token;
            delete $localStorage.loggedInUser;
            $location.path('/login');
        };

        this.getUser = function (token) {
            var user = {};
            if (typeof token !== 'undefined') {
                var encoded = token.split('.')[1];
             var _self = this;
            var encodedUserString = this.urlBase64Decode(encoded);
                user = JSON.parse(encodedUserString);
            _self.set('user',user);
            }
            return user;
        };

        
        this.getTokenClaims = function () {
            var token = $localStorage.token;
            var user = {};
            if (typeof token !== 'undefined') {
                var encoded = token.split('.')[1];
                user = JSON.parse(this.urlBase64Decode(encoded));
            }
            return user;
        };

        this.recoverPassword = function(recoveryEmail){
            var deferred = $q.defer();
            var  url = $rootScope.urls.recoverPassword,
                _self = this;
            dataService.post(url,recoveryEmail).then(function(data) {
                deferred.resolve(data);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };


        // Sends Service worker data to server
        this.sendSUB = function (subObject) {
            var deferred = $q.defer();
            var  url = $rootScope.urls.sendSubUrl;
            dataService.post(url,subObject).then(function(data) {
                deferred.resolve(data);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

    }

]);