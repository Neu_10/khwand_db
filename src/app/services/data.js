Appx.service('DataService',[
    '$http',
    '$q',
    '$log',
    '$rootScope',
    '$localStorage',
    'HelperService',
    '$state',
    function($http, $q, $log, $rootScope, $localStorage, helper, $state)
    {
        /**
         * Makes an API request and returns a promise containing
         * the collection if successful, and an error object if not.
         * Additionally logs any errors to $log
         *
         * @param  {String} type request method
         * @param  {String} url  resourse to request
         * @param  {Object} data data to send if doing a post/put
         * @return {Object}      promise
         */
        var r = function(type, url, data) {
            var deferred = $q.defer();
           // $rootScope.baseURL = 'http://demo7030050.mockable.io/';
            var apiBaseURL = url.indexOf('https://')>-1 ? url : $rootScope.baseURL+url;
            $http.defaults.headers.common['Authorization'] = 'Bearer ' + $localStorage.token; // jshint ignore:line
            $http[type](apiBaseURL,data).success(function(data) {
                if(data!= null && data.collection != null && data.collection.error) {
                    var err = data.collection.error;
                    deferred.reject(err);
                    $log.error('API Error:', err);
                } else {
                    deferred.resolve(data);
                }
            }).error(function(data, status, headers, config) {
                var request = config.method + ' ' + config.url,
                    err = 'API request ' + request + ' failed with response code ' + status;

                var user = {};
                /*$localStorage.token = data && data.jwt ? data.jwt : $localStorage.token;
                user = helper.getUser($localStorage.token);
                if(user.role){
                if(status === 401){
                    if (user.role === "appx_admin") $state.go('admin.unauthorized');
                    else if (user.role === "rest_admin") $state.go('rest.unauthorized');
                    else $state.go('login');
                } else if(status === 404){
                    if (user.role === "appx_admin") $state.go('admin.notfound');
                    else if (user.role === "rest_admin") $state.go('rest.notfound');
                } else if(status === 500){
                    if (user.role === "appx_admin") $state.go('admin.servererror');
                    else if (user.role === "rest_admin") $state.go('rest.servererror');
                }
                }else{
                    $state.go('login');
                }*/


                deferred.reject(data);

                $log.error('API Error:', err);
            });
            return deferred.promise;
        };

        this.get = function(url) {
            return r('get', url);
        };

        this.post = function(url, data) {
            return r('post', url, data);
        };

        this.put = function(url, data) {
            return r('put', url, data);
        };

        this.delete = function(url) {
            return r('delete', url);
        };

    }
]);
