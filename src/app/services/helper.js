
Appx.service('HelperService', [
    '_',
    function(_)
    {

        var privates = {};
        privates.apiData = {};

        this.get = function (item) {
            return privates[item];
        };
        this.set = function (item, val) {
            privates[item] = val;
        };

        /**
         * Map an array of name/value objects to a
         * flat key/value object where the name property
         * is used as the key
         *
         * @param  {Array} arr
         * @return {Object}
         */
        this.getObjFromArray = function(arr) {
            return _.zipObject(_.map(arr, 'id'), _.map(arr, 'name'));
        };

        /**
         * Groups an array of objects by a common
         * sub-property key
         *
         * @param  {String} prop  key to group by
         * @param  {Array}  items input array
         * @return {Array}        grouped array
         */
        this.groupItemsBy = function(prop, items) {
            var arr = [], _self = this;

            _.each(items, function(item) {
                arr.push(_self.getObjFromArray(item.data));
            });

            return _.chain(arr).groupBy(prop).map(function(key, value) {
                return { id : value, services : key };
            }).value();
        };

        /**
         * Takes an array of data nodes and returns a new array
         * with each node point mapped as an object keyed to its
         * original "name" property, allowing easier traversal.
         * Also maps adjacent "links" array to a child property
         * of the node, if it exists.
         *
         * @param  {Array} nodes original nodes array from API
         * @return {Array}       mapped node array
         */
        this.formatNodes = function(nodes) {
            var formatted = [];
            _.each(nodes, function(node) {
                var i = _.indexBy(node.data, 'name');
                i.links = node.links;
                formatted.push(i);
            });
            return formatted;
        };

        /**
         * Returns a specified property for a given "name"
         * key within a node's data array
         *
         * @param  {Array}  node node array
         * @param  {String} name property name
         * @param  {String} prop property to return
         * @return {Multi}       matched property value
         */
        this.getNodePropValue = function(node, name, prop) {
            if(!node.data || !node.data.length) {
                return false;
            }
            for(var i = 0; i < node.data.length; i++) {
                if(node.data[i].name === name) {
                    return node.data[i][prop];
                }
            }
            return false;
        };

        this.setNodeResponse = function(tmpl, name, rawanswer) {
            if(!tmpl.data || !tmpl.data.length) {
                return false;
            }
            for(var i = 0; i < tmpl.data.length; i++) {
                if(tmpl.data[i].name === name) {
                    tmpl.data[i].value = rawanswer.value;
                    tmpl.data[i].prompt = rawanswer.prompt;
                }
            }
            return tmpl;
        };

        /**
         * Takes an array of data nodes and returns a new array containing
         * the nodeId and nodeValue properties for each "editable" node type
         * found. "Editable" in this case refers to any node whose values
         * can be modified by the user, e.g. form field data
         *
         * @param  {Array} nodes Array of data nodes
         * @return {Array}       Array of editable data nodes
         */
        this.getEditableNodes = function(nodes) {
            var editableNodes = [
                    'statement',
                    'choiceQuestion',
                    'selectQuestion',
                    'editableField',
                    'collectQuestion',
                    'textArea'
                ],
                retObj = {},
                _self = this,
                nodeType, id;
            _.each(nodes, function(node) {
                nodeType = _self.getNodePropValue(node, 'nodeType', 'value');
                if(_.indexOf(editableNodes, nodeType) !== -1) {
                    id  = _self.getNodePropValue(node, 'nodeId', 'value');
                    if(id) {
                        retObj[id] = _self.getNodePropValue(node, 'nodeValue', 'value');
                    }
                }
            });
            return retObj;
        };

        /**
         * Takes a collection of scope formItems and applies then
         * to the relevant template fields then returns the new template
         *
         * @param  {Object} tmpl     template to apply fields to
         * @param  {Object} answers  Scope formItems
         * @return {Object} template
         */
        this.setTemplateAnswers = function(tmpl, answers) {
            if(!tmpl.data || !tmpl.data.length) {
                return false;
            }
            _.each(tmpl.data, function(item) {
                if(_.has(answers, item.name)) {
                    item.value = answers[item.name];
                }
            });
            return tmpl;
        };

        /**
         * Returns true if a given node contains a link with
         * a rel property matching the supplied argument
         *
         * @param  {Object} node Node to look for links in
         * @param  {String} rel  string to match
         * @return {Boolean}
         */
        this.nodeHasLinkOfRel = function(node, rel) {
            if(!node.links) {
                return false;
            }
            return _.some(node.links, function(link) {
                return link.rel === rel;
            });
        };

        /**
         * Searches an array of links for one with a matching rel
         * and returns the value of prop, if found (assumes unique
         * rel value in array)
         *
         * @param  {Array}  links  array of nodes
         * @param  {String} rel    value of rel property to match
         * @param  {String} prop   key of prop to return
         * @return {Multi}         value of matching prop key
         */
        this.getLinkPropByRel = function(links, rel, prop) {
            for(var i = 0; i < links.length; i++) {
                if(links[i].rel === rel) {
                    return links[i][prop];
                }
            }
            return false;
        };

        this.getAnswerTextFromId = function(item, id) {
            var options = this.getNodePropValue(item, 'nodeOptions', 'options');
            for(var i = 0; i < options.length; i++) {
                if(options[i].value === id) {
                    return options[i].prompt;
                }
            }
            return false;
        };

        this.nextLocation = function(flowType, flowState, step) {
            if (flowType === 'SupplierFault-flow'|| flowType === 'Appointing-flow' || flowState === 'appointedFaultExit' || flowState === 'nonAppointedFaultExit') {
                if (flowState === 'chooseAppointment') {
                    $location.path('/process/appointment');
                } else {
                    $location.path('/fault_flow/' + step);
                }
            } else {
                $location.path('/questions/' + step);
            }
        };


        this.getUser = function (token) {
            var user = {};
            if (typeof token !== 'undefined') {
                var encoded = token.split('.')[1];
                var _self = this;
                var encodedUserString = this.urlBase64Decode(encoded);
                user = JSON.parse(encodedUserString);
                _self.set('user',user);
            }
            return user;
        };


        this.urlBase64Decode = function (str) {
            var output = str.replace('-', '+').replace('_', '/');
            switch (output.length % 4) {
                case 0:
                    break;
                case 2:
                    output += '==';
                    break;
                case 3:
                    output += '=';
                    break;
                default:
                    throw 'Illegal base64url string!';
            }
            return window.atob(output)
        };

    }
]);
