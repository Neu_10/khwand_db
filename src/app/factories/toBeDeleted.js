Appx.factory('redirectInterceptor', ['$location', '$q', function ($location, $q) {
    return function (promise) {
        promise.then(
            function (response) {
                if (typeof response.data === 'string') {
                    if (response.data.indexOf instanceof Function &&
                        response.data.indexOf('<html id="ng-app" ng-app="loginApp">') != -1) {
                        $location.path("/logout");
                        window.location = url + "logout"; // just in case
                    }
                }
                return response;
            },
            function (response) {
                return $q.reject(response);
            }
        );
        return promise;
    };
}]);