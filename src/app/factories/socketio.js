
Appx.factory('SocketIO',[
    '$rootScope',
    function ($rootScope) 
    {
        // var socket = io.connect('localhost:3000');
        //var socket = io.connect('http://159.203.104.64:3000');
        return {
            on: function (eventName, callback) {
                socket.on(eventName, function () {
                    var args = arguments;
                    $rootScope.$apply(function () {
                        callback.apply(socket, args);
                    });
                });
            },
            emit: function (eventName, data, callback) {
                socket.emit(eventName, data, function () {
                    var args = arguments;
                    $rootScope.$apply(function () {
                        if (callback) {
                            callback.apply(socket, args);
                        }
                    });
                })
            },
            disconnect : function () {
                socket.disconnect();
            }
        };

    }//function
]);
