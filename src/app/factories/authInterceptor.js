/**
 * Created by Nutan on 6/5/2015.
 */
Application.instance
    .factory('AuthInterceptor',['$window','$q','$location','$localStorage', function ($window,$q,$location,$localStorage) {
        return{
            request : function (config) {
                config.headers = config.headers || {};
                config.headers =  {
                    'Content-Type': 'application/json',
                    'Authorization' : 'Bearer ' + $localStorage.token
                };

                if($window.sessionStorage.getItem('token')){
                    config.headers.Authorization = 'Bearer '+$window.sessionStorage.getItem('token');

                }
                return config || $q.when(config);
            },
            response : function (response) {
                console.log('In response ');
                if(response.status === 401){
                    $location.path('/login');
                }
                return response || $q.when(response);
            },
            responseError : function (rejection) {
                console.log('In Rejection ');
                if(rejection.status === 401 || rejection.status === 0){
                    $location.path('/login');
                }
                return $q.reject(rejection);
            }
        };
    }]);

